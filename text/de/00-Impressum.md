<div class="impressum">
    <strong>Bitbucket</strong>:
    <!-- if booktype is epub --><a href="${BOOK_REPOSITORY}">iris-johannson-irrgefuhl</a><br /><!-- endif booktype -->
    <!-- if booktype is pdf --><span>${BOOK_REPOSITORY}</span><!-- endif booktype -->
    <br />
    <!-- if booktype is epub --><strong>ASIN</strong>: ${BOOK_ASIN}<br /><!-- endif booktype -->
    <!-- if booktype is pdf --><strong>ISBN</strong>: ${BOOK_ISBN}<br /><!-- endif booktype -->
    <strong>Version</strong>: ${BOOK_VERSION}<br />
    <strong>Lizenz</strong>: ${BOOK_LICENSE}<br />
    <strong>Mitwirkende</strong>:<br />
    Filip Wall (Übersetzung)<br />
    Michael Schmidt (Herausgabe)<br />
</div>

<div class="title-page">${BOOK_TITLE}</div>
<div>&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

## Vorwort

Liebe Leserin, lieber Leser,

dieses Buch ist ein Versuch.
Ein Versuch, ein Thema, das „unter den Nägeln brennt“, auf eine andere Weise anzugehen.
Ein Versuch, von denen es viele andere gibt, Gott sei Dank viele andere gibt.
Dieser Versuch geht aus von der speziellen Atmosphäre, die Iris in ihren Gesprächskreisen schafft, einer Atmosphäre, die es leicht macht, so zu sein wie man ist, ohne Angst, ohne sich verstecken zu müssen.
Manche Menschen kommen jahrelang zu ihren Kursen nur wegen dieser Atmosphäre.

Die Idee dieses Buches ist, etwas davon einzufangen, es zwischen und hinter den Zeilen da zu haben.
Und dann vielleicht, in der besten von allen Welten, anzuregen, selbst solche Gesprächskreise ins Leben zu rufen und im gegenseitigen Erzählen und Lauschen die Phänomene zu bewegen, gemeinsam ...
Und dabei entsteht oft Neues, Eigenens, man kann in ein lebendiges Spielen kommen, das gerne ernst werden darf.
Man kann ins Spielen kommen mit den Dingen, die uns im Leben unter den Nägeln brennen.
Und wenn in einem solchen Kreis etwas Neues neu entsteht, zum Beispiel wenn neue Fragen auftauchen, dann wollen wir, das Team vom Garten des Epikur, gerne davon hören und es vielleicht diesem Buch hinzufügen.

Filip Wall, der die Übersetzung aus dem Schwedischen besorgt, lebt in dem Impuls, etwas Unmögliches zu schaffen:
Er will das, was Iris sagt, nicht einfach ins Deutsche übersetzen, und alle die Unterschiede, die die schwedische und deutsche Sprache haben, wirken lassen.
Er will übersetzen und den Text gleichzeitig in seiner schwedischen oder sogar irisischen Eigenart belassen.
Iris Sprache ist nicht ganz Schwedisch, sie verwendet eigene Worte, die auch Schweden erst lernen müssen.
Dass Filip das so macht, habe ich nach den ersten Korrekturen des ersten Buches gemerkt, wo ich versucht habe, aus allem „richtiges Deutsch“ zu machen, was für mich nicht wirklich deutsch war.
Aber Filip übernahm kaum einen meiner Vorschläge.
Ich war verwirrt.
In langen Geprächen habe ich dann mehr über die Tiefe seines Spielens mit dem Text erfahren und korrigiere seit dem mit Sorgfalt nur noch offensichtliche Rechtschreib- und Kommasetzungsfehler.

Es muss nichts perfekt sein, nur gut genug.
Perfektion ist der Moment vom Tod.
Spielen ist Leben.
Spielen ist Menschsein.
Ich habe mit den Überschriften gespielt, mit der Einteilung in Kapitel, mit der Gestaltung und vor allem damit, aus den Texten richtige Internet-Projekte zu machen, die so gestaltet sind, dass sie sich ständig verändern können.
Also, wenn Dir beim Lesen Fragen kommen, oder du an einer Stelle gerne weiterfragen würdest, schicke die Frage gerne an sexualitaet@epikur.berlin.
Wir werden sie dann an Iris weiterschicken und ihre Antwort vielleicht ins Buch mitaufnehmen.
Sie ist dann bei der nächsten Veröffentlichung mit dabei.

Spielen ist das, um was es mir in diesem Projekt geht.
Ich will mit Filip spielen, irgendwo zwischen seiner Übersetzung und meinen Kapiteln.
Auf dem Umschlag spielen wir gemeinsam.
Und ich will auch zusammen mit dir spielen, oder mitbekommen, wie du spielst, zu den Themen des Buches, darüber sprechen...

„Was ist erquicklicher als Licht?“ fragt Goethe in seinem Märchen.
Seine Antwort:
„Das Gespräch.“

Michael Schmidt, im März 2021

P.S.

Wer den Text ohne Überschriften und Kapitel lesen will, kann ihn sich hier herunterladen:

<!-- if booktype is pdf -->${RAW_BOOK_URL}<!-- endif booktype -->
<!-- if booktype is epub --><a href="${RAW_BOOK_URL}">${BOOK_TITLE}.rtf</a><!-- endif booktype -->

<div class="title-page">&nbsp</div>
