## Ich will einfach nur leben, ohne Partner, ohne Familie, ist das normal?

<div class="handwritten">
Neue Frage:
Ich verliebe mich leicht in Kunst, in Musik, in Abenteuer und in anderes, aber ich habe keine besondere Sexlust.
Ich lebe in der Kunst und ich führe ein bohemisches Leben und genieße all das, was auf Erden und im Leben ist, aber ich habe keine Sehnsucht nach jemandem, mit dem ich lebe und mit dem ich Kinder kriege.
Ich möchte ganz einfach nur dieses Leben, dass ich lebe.
Stimmt etwas bei mir nicht, da ich keine Familie haben möchte und ein etablierter Teil der Gesellschaft sein möchte?
</div>

Nein, gar nicht.
Der Sinn im Leben ist das Leben, und du lebst und deinen Wert gibt es, indem es dich gibt.
Und das ist ohne Bedingungen so.
Lebe einfach weiter und du wirst sehen, wohin das Leben führt.
Oft wechseln die Dinge, die einem Freude machen, von Zeit zu Zeit.
Bei manchen ist es die Art von Geistesgegenwart im Moment, die das Wesentliche im Leben darstellt, und auch das ist ganz natürlich.
Oft sagen Menschen, dass sie etwas Nützliches im Leben tun müssen, aber wenn man einfach so lebt wie du es tust, ist der Schaden minimal, und es nützt sehr, dass es dies dann als Gegenpol gibt zu all denen, die nach dem Ideal der Zivilisation wesentlichen Nutzen tun.
