## Gefühle werden nicht durch den anderen erzeugt

<div class="handwritten">
Was ist deiner Meinung nach der Auslöser für das Gemeinsein oder den Sadismus, oder dafür, dass jemand einem anderen Menschen bis er zusammenbricht verfolgt und dabei vielleicht das Leben aufgibt?
</div>

Dadurch, dass die Sexualität ein Trieb ist und nicht in Verbindung mit Bewertungen gebracht wird, wird sie aus dem Lust-Unlust-Prinzip, das von dem Reptilienhirn ausgeht, heraus gesteuert, springt in allen geeigneten und ungeeigneten Situationen an und weckt sehr tiefe und ursprüngliche Reaktionen in uns auf, z.B. Eifersucht.

Es wirkt wie bei den eigenen Kindern... wenn etwas sie von außen her bedroht, was dazu führen kann, dass ich als Elternteil mein Kind verliere, kommen starke, aggressive Verteidigungs- und Schutzinstinkte herauf, die zu unsozialen Handlungen führen.
Das heißt, denjenigen zunichte zu machen, der jemanden aus meiner Herde angreift.
Oft gibt es Überbleibsel aus der Kindheit der Menschheit, wo es immer um Leben und Tod ging, und weshalb dort das Prinzip herrschte, Auge um Auge, Zahn um Zahn.
Dass jemand so anfing, konnte ein ganzes Geschlecht vernichten, um dann über Generationen so weiterzugehen.
Oft wurde Frieden dadurch gestiftet, dass jemand seine Tochter dem Sohn einer anderen Familie hingab oder umgekehrt und durch die Ehe hörte das Töten auf.

Bei der modernen Eifersucht geht es um Monogamie.
Wir behalten die Sexualität eines anderen uns selber vor und haben dabei Vorstellungen, was es heißt: "Du sollst keine andere Sexualität neben mir haben".
Nicht einmal Lust oder Gefallen an einem anderen, denn dann bin ich verletzt und gekränkt und ich fühle wie wenn du mich mit Füßen trittst.
Das Schwierige daran ist, dass wir mehr und mehr und mehr hinzufügen, was wir als Untreue gegenüber der Monogamie deuten könnten.
Wir können es darauf anlegen, solche Gekränkheitsgefühle zu sammeln, bis wir finden, dass wir sie eintauschen müssen, indem wir die Beziehung beenden, und der andere unwissend bestraft wird wegen etwas, an dem er überhaupt kein Anteil hat.
Es geht nur vor sich im Gefühlsleben eines Menschen.
Dieser Mensch wird bestraft, indem er durch seinen Partner verlassen wird.
Ich nenne das den Konflikt mit Trennung zu lösen, indem die Beziehung beendet wird, und zur nächsten zu gehen und dort das gleiche Muster zu wiederholen, anstatt die eigenen Konflikte zu lösen.

Konflikte entstehen und gehen vor sich in einem selber und haben nichts mit dem anderen zu tun.
Auch wenn wir oft sagen: Du machst mich traurig.
Aber es geht nicht.
Der andere kann die eigenen Gefühle nicht erschaffen, sondern „du tust etwas, weshalb ich traurig werde“, „du tust etwas, weshalb mir Freude ist“. Innen ist dies ein meilenweiter Unterschied.

Wir glauben, dass es etwas Allgemeines ist, was verletzt.
Aber das ist es nicht.
Das ist höchst spezifisch und kommt davon wie unsere eigenen Ängstesysteme aussehen und wovon wir geprägt worden sind, davon, wie unsere Eltern einander behandelt haben.
Das sind die stärksten Strukturen, aus denen her wir bestimmt werden und durch die wir geprägt worden sind.

So lange es um dieses geht, ist es oft möglich, durch Reden darüber zurechtzukommen.
Jeder Einzelne kann sich Hilfe holen, um bei dem Verständnis anzukommen, dass Liebe kein Produkt ist, sondern ein Zustand, und dass es keinen Mangel an Liebe gibt, nur Mangel an dem Fähigsein, dieses zum Gefühl heranzuziehen.
Wir können verstehen, dass es nötig ist, Liebe zu pfegen und durch uns selber aufzunehmen, dass sie in die Zufriedenheit innen hineinverschmelzen braucht, so dass wir es schaffen, gemütlich gegenüber unseren Partnern zu sein, dass beide sich wohl fühlen, offen, verletzlich und neugierig auf sich selber und einander sind.
Verstehen, dass Ängste der Anfang einer Trennung sind, wenn wir nicht sofort darauf setzen, aus dem Gespenst herauszukommen, und wenn nicht, werden wir den anderen begrenzen wollen.

Was wir stattdessen brauchen ist einzusehen, dass der oder die andere mich gewählt hat und da mit mir geht, und das heißt, dass er oder sie mich will, gerade mich, und dann an diesem Punkt vermutlich sehr vertrauenswürdig ist, jedenfalls ausreichend, bis das Gegenteil sich gezeigt hat.
Und wir hoffen ja, dass es sich nicht zeigt.
Die Kunst besteht also darin, das Gegenteil nicht hervorzurufen, denn das Risiko ist da, dass es uns gelingt und wir es uns selber in der Beziehung kaputt machen.
