## Ich glaube, dass ich nicht tauge und Männer mit mir nur Sex haben wollen

<div class="handwritten">
Neue Frage:
Jedes Mal, wenn ich jemanden kennenlerne, mit dem ich gerne gehen würde, dann meine ich, dass ich nicht tauge, dass die Person nicht mich will, sondern nur Sex haben möchte, dass diese Person mich wie ein Objekt anschaut, und dass ich „weiß“, dass diese Person sich zurückziehen wird aus der Beziehung, und deshalb ist es besser die Beziehung schon von Anfang an zu beenden.
Dies macht, dass ich nur sehr kurze Beziehungen habe, die so aufhören wie ich es mir erwartet habe.
Ich verstehe, dass es selbsterfüllend ist, denn ich werde langweilig, wenn ich meine, dass ich nicht gut genug bin.
Gibt es da etwas, was ich tun kann?
</div>

Es ist gut, dass du einsiehst, dass es an einem Gedankenfehler bei dir liegt und dass es nicht in der Wirklichkeit so ist, dass du nicht gut genug bist.
Dass du für den anderen sehr wohl taugen kannst, aber du wagst es nicht, du selber, in Intimität, und in der Begegnung beiderseitig mit dem anderen zu sein.

Das sitzt vermutlich sehr tief und da brauchst Du Hilfe um zu klären, warum du ein Verbot hast dagegen, du selber zu sein, in der Begegnung zu sein, und ein Verbot hast, dir zu erlauben dich zu verändern, wie wir es tun, wenn wir innen licht und froh sind und das Leben, den anderen und uns selber lieben.
Vermutlich lebst du nach einem Ideal, nach einem Bild, dass du erfüllen möchtest und es gelingt dir nicht, und wenn du das nicht kannst, dann verurteilst du dich selber, oder den anderen, oder die ganze Welt und das Leben.
Das ist ein gefühlsmäßiges Spiel.
Und da bekommst du die wenige Aufmerksamkeit von dir selber, damit du überlebst, aber du bleibst einsam und unglücklich und daher... hole dir Hilfe.
Irgendeine Form des Umwertens in der Tiefe brauchst du.
Schreibe mir, wenn du Tipps haben möchtest.
