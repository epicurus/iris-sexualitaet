## Gegenseitige Anpassung macht das Schöne am erotischen Spielen kaputt

<div class="handwritten">
Es scheint so weit hergeholt, aber wahrscheinlich gibt es diese Verbindung zu der alten Zeit...
aber wir sind doch moderne Menschen und meinst du, dass wir immer noch in dem alten, schiefen Denken stecken, und kannst du noch etwas darüber sagen, welche Folgen das heute hat?
</div>

Das, was Frauen oft Probleme macht, glaube ich, ist, dass sie auf irgendeine Weise immer noch sich an den Gedanken anpassen, dass der Mann Samenerguss bekommen muß...
Sie sind da, sie opfern sich, oder sind zu Diensten, sie strengen sich wirklich an, wie wenn das Natürliche nicht wäre, dass der Mann seinen Akt mit Samenerguss abschließt...
Immer noch meint die Frau, dass es an ihr liegt, wenn der Mann impotent wird, und das führt die Männer dazu zu glauben, dass die Frauen es auf die Weise haben wollen, dass sie so sind und das kultiviert die Sichtweise bei der Frau auf sich wie auf ein Objekt, und der Mann hält seinen vererbten Blick auf die Frauen bei.

Es steckt sehr tief und ist auf Zellebene mit Festigkeit geprägt und es ist schwer da rauszukommen, auch wenn wir die Aufklärung im Kopf haben.
Viele Frauen suchen mich auf und sagen...'hilf mir da raus, wenn ich mich meinem Mann unterordne und wie ein Kind werde, wenn er schlechter Laune ist... ich tue mir Gewalt an, ich tue alles, was ich kann, damit er da rauskommt, ich schlafe sogar mit ihm vor dem Essen, auch wenn ich gar nicht Lust dazu habe.
Ich habe nur Hunger.
Und ich tue so, wie wenn ich es wäre, die es will.
Ich könnte mich umbringen.
Dann kommt Verachtung ihm gegenüber auf, weil er so dumm ist, dass er drauf reinfällt.
Auch wenn ich weiß wie es wäre, wenn ich autonom bin, so schaltet sich dies ein.
Iris... ich möchte da rauskommen, denn ich fange an, Männer und Sex und mich selber und auch meinen Mann zu verabscheuen und ich will nur weg... will die Scheidung... will ihm dieses Elend mit mir ersparen... was soll ich bloß tun???? Und es macht da nicht halt... wenn ich etwas haben möchte, wovon ich weiß, mein Mann will eigentlich nicht, dass ich das kriege, so kann ich verführerisch sein und es trotzdem bekommen.
Er 'schmilzt' immer dahin, wenn ich so bin... iihhh, wie ich mich selber deshalb verabscheue...'

<div class="handwritten">
Oh, ich kenne das gut von mir.
Ich weiß, dass meine Mutter bei dem mitgemacht hat, weil sie wusste, dass mein Vater dann immer sanft und bereitwillig wurde und da konnte sie ihn um ihren Finger wickeln, und es so haben wie sie es wollte.
Schon als Kleines dachte ich, ich würde das so nie machen, wenn ich sie wäre.
Aber ich bin davon geprägt, ich verstehe, dass das so ist.
Wie hilfst du jemandem, der in dem feststeckt?
Was hilft einem aus der Versuchung heraus?
</div>

Wenn wir in einer Paarbeziehung leben, dann haben wir, genau wie du, meistens diese Erwartungen aufgenommen.
Sie wirken bei uns wie ein ungeschriebenes Gesetz und wir meinen immer noch, dass wir es sind, die den Mann zufriedenstellen sollen.
Das meint der Mann auch oft und dadurch sind wir uns oft zutiefst darin einig.

Es gibt bei dem eine subtile Anpassung an eine Vorstellung, was wir tun sollen, und auch andersherum, was wir nicht tun sollen... Dafür oder dagegen, aber es ist genauso angepasst... der Mann soll die Bedürfnisse der Frau zufriedenstellen.
Er soll für sie 'alles' tun, er soll auf sie schauen, als wäre sie ein edler Stein, eine Madonna, er soll sie anbeten usw.
In beiden Fällen schwindet die Lust aufgrund dieser Anpassung dafür oder dagegen.

Am Anfang einer Beziehung, wenn das Paar verliebt ist, da haben beide oft einen Sog, ein Pirren, eine Lust.
Sie spüren Verlangen nacheinander und sie geben sich dem erotischen Spielen hin und sie gehen ganz darin auf, und ein Schimmern in vielerlei Weise entsteht.
Jeder von ihnen tut alles, um dem anderen recht zu sein.
Oft kommt da Leisten-Wollen beim Mann auf eine Weise auf und bei der Frau auf eine andere Weise.
Dieser Gedankenfehler, dass der eine das Bedürfnis des anderen befriedigen kann, schafft eine Leistungserwartung und nimmt dem erotischen Spielen seine Schönheit, vor allem wenn die Verliebtheit vorbei ist.
Es sind viele Menschen, die dann Hilfe suchen, oder sie kompensieren durch Fremdgehen oder Prostitution, vielleicht auch durch andere Suchtgewohnheiten.
