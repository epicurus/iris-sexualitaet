## Stimmt es, dass wer viel Pornografie schaut, schwer von normalem Sex erregt wird?

<div class="handwritten">
Neue Frage:
Ich habe irgendwo gelesen, dass Männer, die viel Pornografie sehen, nur schwerlich von normalem Sex erregt werden.
Ist dies wahr?
</div>

Es kann so sein, dass wenn wir als Menschen uns dem aussetzen, etwas Exzeptionelles oft anzuschauen, wenn wir zum Beispiel Pornos angucken und das viel tun, das gilt nicht nur für Männer, sondern für uns alle, die wir Menschen sind, so wird ein Bild, eine Vorstellung in uns eingekerbt, die zur Wirklichkeit wird, obwohl es nur am Schirm ist.
Wir werden von etwas geprägt, was gekannt und gewohnt und geborgen wird, und da glauben wir, dass dies wahr ist, leider.
Das kann Gewalt sein, können Wörter sein, Schimpfwörter, sexualisierte Sprache, Vorurteile,können Gefühle sein, an die wir uns drankleben und von denen wir in Affekt geraten, und so weiter.
Das ist oft die Grundlage unserer Gedankenfehlern.
Und manchmal umfasst dies so viele Menschen, dass es zu einem Majoritätsmissverständnis wird.
Und da scheint es, wie wenn 'alle' so sagen, und da muss es ja wahr sein.
So ist es nicht.
Es wird nicht eher wahr, weil es viele sind, die daran glauben.
Das Problem wird nur größer.

Daher... es gibt einen Sinn darin, nicht große Mengen an Süßigkeiten zu konsumieren, denn da wird man einen großen Sog nach Zucker bekommen.
Und da findet man nicht, dass es gut schmeckt, das, was nicht gesüßt ist durch den Zucker.
Das gilt auch für Pornografie.
