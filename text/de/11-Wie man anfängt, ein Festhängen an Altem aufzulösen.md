## Wie man anfängt, ein Festhängen an Altem aufzulösen

Wir üben uns auch nicht in der Kunst, den Trieb als die Lebenskraft, die er ist, zu genießen und ihn einfach den warmen Quell der Freude sein lassen, der er ist ... diese Gefühle zu haben.
Sie im Innern da sein zu lassen, sinnlich, uns tief innerlich berührend, und unser Leben zu bereichern.

Wenn wir nun auch anschauen, wie die Sexualtät im öffentlichen Raum beschrieben wird, finden wir zum Beispiel die Beschreibung der Weltgesundheitsorganisation...

<em>
„Die Sexualität wird durch Gedanken, Fantasien, Wünsche, Glaubensvorstellungen, Haltungen, Werte, Verhaltensweisen, Rollen und Beziehungen erlebt und ausgedrückt.
Auch wenn Sexualität alle diese Dimensionen umfassen kann, werden alle nicht erlebt oder ausgedrückt.
Die Sexualität wird von einem Zusammenwirken zwischen biologischen, psychologischen, sozialen, ökonomischen, politischen, kulturellen, ethischen, juristischen, historischen, religiösen und geistigen Faktoren beeinflußt.“
</em>

Kein einziges Wort davon, dass er der einzige Trieb des Menschen und tiefer tierischer Einschlag für die Reproduktion ist.

Es ist vielleicht nicht die Antwort auf deine Frage vorhin, aber es ist ein Umstand, von dem man gerne ein Bewusstsein haben kann und der bereichernd einen Platz in unserem eigenen Denken braucht.
Es ist etwas, worüber wir reden brauchen, um eine gemeinsame Sprache zu bekommen, so dass es leicht fällt, bei verschiedenen Gelegenheiten mit anderen Menschen darüber zu sprechen... es wagen sich zu fragen, wie es mit diesem oder jenem wohl eigentlich ist... in etwa, wie beim Reden von unseren Kindern oder von Gefühlen.

Vor allem Paare brauchen unter sich im Klaren darüber sein.
Ich fange normalerweise mit einer Frage an... „ist es so, dass du oft ein Unbehagen oder einen Widerstand spürst, wenn es dazu kommt, dass dein Partner sexuelle Gefühle und Bedürfnisse hat? Spürst du dann irgendeine Form von Bedrohung oder Angst, was dich dazu bringt, von dir ganz abzusehen und du passt dich dem Mann an, weil er sonst vielleicht aufhört, dich zu lieben? Oder... lehnst du die Situation ab, oder manipulierst du, damit die Situation weggeht?“

Dann mache ich damit weiter... „Du brauchst mir keine Antwort auf diese Frage zu geben, brauchst nur nachzudenken, ob es so ist, dass du einfach nicht dem Menschen entgegenkommen, das Gefühl und das Bedürfnis bejahen, und selber dich erregt werden lassen kannst?“
...wenn das sich so verhält... „dann glaube ich, dass du in einer alten Falle feststeckst, in einem Gedankenfehler, und wenn du je Hilfe bekommen möchtest, da durchzugehen und zu verarbeiten, dann gibt es mich, und ich bin dazu bereit.“

Dann kann es lange dauern bis die Person innerlich sich soweit fühlt, mit mir darüber zu reden.
Aber es führt oft dazu, dass Menschen anfangen, mit ihren Freunden über das Thema zu sprechen, und das ist eine gute Sache, denn da fängt die Entdeckung an, dass weit, weit mehr Menschen in dem feststecken, und schon sich dessen bewusst zu werden, hilft einiges.

Das gilt für vieles, wenn es um die Doppelheit des Triebs geht.
Einerseits dieser einfache, unkomplizierte, physische Beweggrund, und anderseits diese verwickelte Beziehungsgeschichte, zu der es kommt, wenn die Sexualität dabei ist.

Sex ist ein heißes Thema, das in Filmen und in Büchern dargestellt wird.
Es soll den Anschein machen so offen und zulassend zu sein und gleichzeitig sind es so viele öffentliche Geheimnisse, die verdeckt gehalten werden, und wo es einfach verboten ist auch in den intimsten Beziehungen, sie in den Mund zu nehmen.
