## Was kann ich tun, um nicht eifersüchtig zu sein?

<div class="handwritten">
Was kann ich tun, um nicht eifersüchtig zu sein?
Ich merke, dass mir innen ganz finster wird, wenn jemand seine Wertschätzung meinem Partner entgegenbringt und so tut, wie wenn es mich nicht gibt.
Dieser Mensch sieht nur meinen Partner und ist charmant und bezaubernd.
</div>

Geh weg und fange an, mit einem anderen Menschen Zeit zu verbringen.
Interessiere dich für etwas ganz anders, was deine Sinne einnehmen, und was dir gefällt.
Wenn du auf einer Party bist, so schaue dich um nach einem anderen Menschen, mit dem du währenddessen Zeit verbringen kannst und knüpfe wieder an deinen Partner an, wenn er oder sie wieder alleine ist.
Wenn Musik zum Zuhören da ist, die du genießen kannst, so gebe dich der hin oder finde jemanden, mit dem du tanzen kannst.

Wenn Kinder in der Nähe sind, so gebe dich dem gemeinsamen Spielen mit ihnen hin.
Ich weiß, dass du zu nichts von all dem, was ich vorschlage, Lust hast, sondern einfach dem Menschen die Augen aus dem Kopf rausreißen möchtest, dem, der deinen Partner so offensichtlich schätzt.
Aber es ist das, von dem du dich wegzwingen sollst... und ich gebe lediglich einige Beispiele, wie du dich wegzwingst.
Dann ist es wichtig, dass du deine Eifersucht nicht bei deinem Partner rauslässt, denn das ist der Anfang vom Ende eurer Beziehung.
Das erschreckt, und ein Mensch der Angst bekommt, wird früher oder später in die Anpassung gehen und das erträgt der Mensch nicht.
Eifersucht auf diese Weise sind nur Hirngespinste, du meinst, dass der andere etwas bekommt, das, was du schenkst, ersetzt.
Das ist ein Besitztum-Denken, das in einer Beziehung keinen Platz hat, nicht einmal wenn wir ein Paar sind.
