## Wie kann man nach Inzest-Erlebnissen den Weg zu einem natürlichen Sexualleben finden?

<div class="handwritten">
Nächste Frage.
Hier kommen einige Fragen, die davon handeln, seinem Trieb ausgeliefert zu sein.
Kannst du sie auf eine Weise beantworten, die eine konkrete Hilfe sein kann?
Wenn man dieses ganze Heftchen gelesen hat, dann versteht man vielleicht diese Fragen selber auf eine tiefere Weise und kann die Ratschläge aufnehmen, die du anbietest, damit die Personen aus ihren eigenen, unbeweglichen Positionen rausgehen.
Ist das möglich?
</div>

Ich kann meistens etwas sagen, was ein Schlüssel sein kann, mit dem man weiter suchen kann.
Vielleicht so, dass man eine eigene Frage formulieren kann, an die die Person nicht früher gedacht hat, und so weiter.

<div class="handwritten">
Wenn man Inzest erlebt hat und keine Sexlust spüren kann, weil die Erwachsenen ekelig sind in den eigenen Augen, kann man durch Zuhören Hilfe bekommen.
</div>

Da ein Abo zu haben, damit man anrufen kann, wenn man es braucht.
Den Ekel zu entladen, kotzen darüber und schreien, brüllen, verteufeln, weinen und so weiter.
All das, was nach dem Übergriff noch da ist, und all das, was um einem als man klein war geschehen ist.

Jemanden, der eine Atmosphäre hat, so dass man mit jemandem in Kontakt sein kann über das Telefon, mit einem unbekannten Menschen, der sonst nicht mit dem eigenen Leben zu tun hat, das ist der beste Weg, Abstand zu den Inzestübergriffen zu bekommen.
Ich kann so einen Kontakt vermitteln, wenn der Bedarf da ist.

<div class="handwritten">
Wenn die Lust verschwindet, weil man als Kind Gewalt und Übergriffen ausgesetzt war.
</div>

Schaue das geradeeben Gesagte an:
Man braucht da die gleiche Hilfe, als eine Ergänzung der Bearbeitung der Gelegenheiten, die schockiert haben, was Auslöser des Ganzen war und das, was dann zu posttraumatischem Stress geworden ist.
Und man braucht das, was ich Primärarbeit nenne, um zum Erwachsenen innen heranzuwachsen.

<div class="handwritten">
Sex als Sucht.
Wenn man Süchtiger ist, was tut man da, wie kommt man da raus?
</div>

Sucht ist ein Sonderkapitel, wenn es um eine leicht brechende Stelle in der Konstitution geht.
Man braucht da zu lernen, als unbrauchender Süchtiger zu leben.
Und es ist möglich, auch damit zufrieden zu sein.
Ganz gewiss braucht man von außen Hilfe.

<div class="handwritten">
Wenn die Lust fernbleibt, nachdem etwas geschehen ist, was einem Gewalt angetan hat, in etwa wie bei der metoo-Bewegung, wie wird man damit fertig?
</div>

Wenn es im Erwachsenenalter geschehen ist, auch bei jungen Erwachsenen, ist es irgendeine Form von Therapie, die hilft.
Nicht so sehr die Methode, sondern die Person, die von der Methode Gebrauch macht.
Es sind Gespräche, und die Prozesse, die man macht mittels den Wörtern, und die Reaktionen, die das Ganze auslöschen und helfen, so dass es zu Geschichte wird, die man hinter sich legen kann.

<div class="handwritten">
Du konntest kurze Antworten geben, die nur Ratschläge sind, um anfänglich etwas in die Hand zu nehmen und das hat auch seinen Platz.
</div>

Hier kommt eine Antwort, die etwas länger ist, und die auch eine kurze Analyse ist.
Das Schwierige an Inzest ist, dass es oft die Person ist, von der man die meiste Liebe spürt, der oder die einen dann sexuell ausnützt, und das passt nicht zusammen.
Kinder fühlen, dass es Unrecht ist, aber sie verstehen nicht, was falsch ist, und tun, was der Erwachsene möchte, weil Kinder meinen, dass es auf die Weise so ist wie es sein soll.
Daher wird es so schwer, wenn man selber geschlechtsreif wird und erotische Empfindungen hat, weil sie quasi nicht mit einem selber zusammengehören, sondern nur mit dem anderen, und an der Stelle entsteht dann oft ein Ekel.
Der Mensch, der diesem ausgesetzt wurde, braucht jemanden, mit dem er reden kann, oder eher jemanden, der ihm zuhört, wenn er von all den doppelten Gefühlen erzählt, wenn er reagiert und entlädt.
Er holt sie zum Hier und Jetzt herauf und lebt sie durch, im Anschluss an die erwachsene Vernunft, bis die Entladung das Inzestuöse zur Geschichte werden lässt, und sie dann nicht mehr auf Beziehungen der Gegenwart aktiv einwirken.
