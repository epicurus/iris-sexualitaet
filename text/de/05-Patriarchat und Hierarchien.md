## Patriarchat und Hierarchien

Danach entstanden organisierte Gemeinschaften auf einer anderen Basis.
Es gab verschiedene Geschlechter, gewisse Familien, die Landbesitz erschufen, und indem sie das taten, konnten sie Reichtum erschaffen.
China hatte eine solche Zeit seiner Größe, Japan auch, sowie das Griechenland des Altertums, und Rom auch.
Später entstand das Ein-Gott-System und das hatte seinen Ursprung in dem Gebiet, wo heute Iran und Irak liegen.
Das Volk dort wurde Perser genannt und ihre Lehre war der Zoroastrismus.

Das wurde durch das jüdische Volk übernommen und hat das Gottessystem geschaffen, das heutzutage in großen Teilen der Welt vorherrschend ist, ein System, das auf Dogmas aufbauend den Platz des Primären belegt hat und unter anderem das Patriarchat und die Hierarchie geschaffen hat.
Es ist interessant zu verstehen, dass es gerade diese dogmatischen hierarchischen Systeme waren, die das Griechenland des Altertums auf so eine Weise verwundbar machten, dass Rom über es herfallen konnte.
Das gleiche gilt für das Ein-Gott-System.
Es wird auch verwundbar, weil das Geschlecht dem Menschlichen vorausgeht und die Anpassung daran die Stärke eines Systems bricht.

Dass die Sexualität ein so großes Geschäft geworden ist, liegt an den heiligen Texten, und an der Angst vor der autonomen Freude und der autonomen Angstbefreiung, die da ist, wenn zwei Menschen in sexueller wonniglicher Lust, in sensitivem und erotischem Kinderzeugen zusammenkommen.
Und selbst wenn Letzteres verhindert wird, so gibt es beiderseitigerweise im Trieb die beiden anderen autonom befreienden Kräfte.

<div class="handwritten">
Es klingt sehr schön, das was du zuletzt gesagt hast, aber ich brauche mich damit zu befassen, denn das habe ich noch nicht getan.
Aber ich verstehe, dass es anders war als das Menschengeschlecht sich noch nicht zivilisiert hat und das ist ein schönes Gefühl, wenn ich mir davon ein Bild mache.
</div>
