## Wie verhält es sich mit der Sexualität bei Menschen mit Autismus und mit geistiger Behinderung?

<div class="handwritten">
Nächste Frage:
Wie verhält es sich mit der Sexualität bei Menschen mit Autismus und mit geistiger Behinderung?
</div>

Der Trieb wird durch Autismus und durch geistige Behinderung sehr wenig beeinflusst.
Im Grunde ist er freier bei ihnen, weil sie nicht eine Menge sozialer Hemmnisse und Begrenzungen haben.
Deshalb wird der Trieb oft zum Problem für Eltern und für Pflegepersonal, weil sie möchten, dass der Trieb normal gehemmt und zurückgezogen sein soll, auch bei den Menschen mit Entwicklungsvariationen.
Dadurch ist er natürlich, aber braucht die Regulierung, und das wird derjenige oder diejenige nicht verstehen.
Aber manchmal ist es möglich, neu zu programmieren, und stattdessen in ein anderes Verhalten umzuwandeln... manchmal.
