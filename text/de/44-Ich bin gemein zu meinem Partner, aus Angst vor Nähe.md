## Ich bin gemein zu meinem Partner, aus Angst vor Nähe

<div class="handwritten">
Wenn man dem eigenen Partner gegenüber gemein ist, weil vor der Nähe eigentlich Angst da ist und es wird versucht auf Abstand zu gehen oder die Beziehung zu beenden, obwohl es das eigentlich nicht ist, was man will, was kann man da tun?
</div>

Hol dir selber sobald du kannst Hilfe, und geh zu jemanden, der dir helfen kann zu verstehen, dass dein Autopilot, dein Schutz- und Verteidigungssystem fehlprogrammiert ist.
Erzähle es deinem Partner auch, und sag' ihm wie es ist, dass du nichts lieber möchtest, als Zeit mit ihm zu verbringen und dabei lieb und warm und nett zu sein und dass du keinen Einfluss darauf nehmen kannst, dass du dich stattdessen gemein wie eine Hexe verhältst.
Dass du den Wunsch hast, dass dein Partner oder deine Partnerin es trotzdem mit dir aushält, bis du mit deinem Problem zurecht gekommen bist, und es ihm oder ihr frei steht, wütend oder traurig zu werden oder Angst zu bekommen, oder was auch immer zu werden und dich einfach auszulachen.
Und dass du den Weg findest dazu, so zu werden wie du bist, ohne das Anspringen deines Ängstesystems.
Wenn ich Dich kennen würde, würde ich zu dir sagen, dass du an einer Lebenspielgruppe teilnehmen sollst, weil das ein Weg ist, es in der Tiefe zu verändern, und als Ergänzung dazu ein Zuhörerabonnement zu haben, das ich vorher beschrieben habe.
Dieses Verhalten sitzt zu tief, als dass es möglich ist, mit Wörtern, mit Tricks, mit Methoden oder Techniken dranzukommen.
Das sitzt auf primärer Ebene in deinem intuitiven System.
Und alles Heilen und alles Kurieren macht der Körper selber, wenn er die richtigen Voraussetzungen bekommt.
Ohne Erwartung, einfach geradewegs hinein in das Problem gehen und schauen was passiert und sich verändern lassen.

Nun gut, jetzt habe ich auf alle deine mitgebrachten Fragen auch eine Antwort gegeben.
Reicht es so oder gibt es etwas anderes, an das du denkst, und was du aus meiner Perspektive im Moment gespiegelt haben möchtest?

<div class="handwritten">
Es ist soweit gut und ich verstehe, dass ich das Ganze von vorne lesen brauche, damit die kurzen Antworten veständlich werden.
Es ist nicht einfach, das eigene Triebleben zu verstehen.
</div>

Doch, im Grunde ist es sehr einfach, aber es ist nicht leicht und in der sekundären verumständlichten Zivilisation wird es immer komplizierter weil es schnell passiert, dass sie den Platz des Primären einnimmt.

Meine E-Mail-Adresse: iris@irisjohansson.com.
