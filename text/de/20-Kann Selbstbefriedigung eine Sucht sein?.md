## Kann Selbstbefriedigung eine Sucht sein?

<div class="handwritten">
Ich habe von einem Mann eine Frage bekommen, die ich dir gerne stellen würde:
„Ich habe mich während meiner Teenagerzeit sehr viel selber befriedigt.
Das war meine Art, Leben in mir zu spüren.
Aber ich meine, dass ich auch abhängig davon wurde.
Wie kann ich heute mit der Geschichte leben?
Wie soll ich tun, wenn dieser Sog in mir anfängt?
Ich kann mich ja nicht ganz und gar den Frauen entziehen...“
</div>

Es ist oft so, dass Kinder sich gegen Tischbeine, Sessel, Matratzen und so weiter 'reiben'.
Das ist nicht Selbstbefriedigung, das ist empfindungsmäße, sinnliche Erlebnisse der Berührung.
Und sie kommen dadurch zur Zufriedenheit in sich selber.
Oft wenn sie müde oder überreizt sind, dann landen sie in sich selber und fühlen sich wohl.

Wie du, der fragt, sagst, während der Teenagerzeit wenn du geschlechtsreif wirst, so fängst du an, den Genuss der Selbstbefriedigung zu entdecken, und es ist ein Weg Spannungen loszulassen, Unsicherheit und Irrgefühle über diese ganze fremde Welt zu lindern, die in der Pubertät aufgeht, und dann noch der Gefühlsstrom, der durch den Trieb heraufkommt, der lockt, verzaubert und erschreckt.
Wenn man dann sich selbst befriedigt, ist es ein Weg zu sich selber zu kommen.
Danach ist ein schönes Gefühl der Entspannung da und man schläft gut ein oder man fühlt sich am Morgen wach.

An sich ist das harmlos und unschädlich, aber wenn man darin wie in einem Zwang stecken bleibt, dann wird es für einen selbst und vielleicht... für die Umgebung zum Problem.
Was man da über sich selber erfahren kann, ist, dass man eine latente Suchtneigung hat.
Und wenn man das versteht, ist es einem vielleicht möglich, gefährlichere Suchtarten als diese zu meiden.
Es ist wichtig, Hilfe zu holen, jemandem davon zu erzählen, dem man vertraut, und dann Hilfestellung bekommen, wie es möglich ist, mit diesem brüchigen Teil der Konstitution zu leben.

Es ist wichtig für dich zu wissen, dass es sehr normal ist.
95 Prozent aller Männer befriedigen sich selber mindestens einmal pro Woche.
Und bei den Frauen ist die Zahl 72 Prozent.
Deshalb ist es im Grunde kein Problem und du brauchst das nicht als etwas Schuld-Schamhaftes ansehen, zu dem du dich bekennen mußt... aber wenn es zum Zwangsverhalten wird, wenn es zu einem Wiederholungszwang bei dir wird, dann ist es ein Problem, und dann brauchen wir anzuschauen, was du durch die Selbstbefriedigung zu entkommen oder in dir selber zu lösen versuchst.
