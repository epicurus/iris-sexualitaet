## Die Kunst besteht darin, nichts zu tun

Du fragst, was wir stattdessen tun sollen und diese Frage ist eine sehr gute, denn die Kunst besteht darin, nichts zu tun... einfach beim Spielen sein, es sich tun lassen, was sich tut, manchmal gut, manchmal weniger gut... dass es bei uns ein Spielen ist, bei dem wir sind, das uns freut, das schlichthin ein Genuss ist, oder bizarr ist und so weiter.
Genauso wie ursprüngliches Spielen ist, es entsteht, es geht vor sich bis es nicht mehr möglich ist.
Im Fall vom Sex kann dies sein, wenn der Samenerguss der Frau oder dem Mann die Lust verschwinden läßt.

Jeder weiß, dass die Lust immer wieder und wieder kommt.
Aber gerade jetzt ist sie nicht da und dann tun wir in der Zeit etwas anderes.

Wenn wir als Menschen darauf vertrauen können, dass Sex ein Trieb ist, und dass er von selber von innen kommt, wenn wir in uns selber, in dem Sein sind, und das geschieht von alleine.
Keiner der beiden muss denken oder beurteilen, sie brauchen nur da zu sein und die Nähe des anderen genießen.
Gerade dies, in sich selber sein ohne Vorbehalte, und dann in diesem Sein sich miteinander bewegen, wie ein Tanzen, wie ein Spielen, wie ein Beisammensein und einfach genießen.
Dann wird es autonom... dann wird es wie es in Wirklichkeit ist ... ein Liebesspiel.

Ich habe ein wenig nach einer sachlichen Beschreibung gesucht, was Sexualität ist, und da fand ich eine.
Sie gebraucht so viele Wörter.
Aber es gibt dort nicht einmal eine Andeutung, dass sie ein Trieb ist.
Ich werde die Beschreibung hier anführen, damit du verstehst, wie verwickelt die Sache in dieser Zivilisation ist, dass der Mensch durch diesen Trieb gesteuert wird, wenn eine globale Einrichtung den entscheidendsten Faktor, wenn es um die Sexualität geht, bei ihrer Beschreibung nicht dabei hat... dass sie ein TRIEB ist und dass dies heißt, dass sie nicht über das Bewusstsein geht, um sich anzuschalten, sondern sie geht direkt vom Reptilienhirn - oder vom Kleinhirn - zum Impuls-Handlungs-System.

Dass dies der Grund dafür ist, dass wir so viel Angst vor dem Trieb bekommen, und dass die Sache in uns sich so arg verwickelt, statt einfach einige einfache Tabus zu haben, von denen alle wissen und denen alle folgen, vorbehaltslos wie Naturgesetzen „So tut man einfach nicht“... vorbehaltslos und ohne Anpassung.
