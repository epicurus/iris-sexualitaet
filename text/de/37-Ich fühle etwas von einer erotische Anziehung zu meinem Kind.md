## Erotische Gefühle bei der Mutter wenn sie bei ihrem kleinen Kind ist

<div class="handwritten">
Nächste Frage:
Ich habe gespürt, wie eine erotische Natur in Beziehung zu meinem kleinen Kind anspringt.
Ist das irgendwie krank?
</div>

Nein, gar nicht.
Dass wir solche Lustgefühle von Musik bekommen, von der Kunst, vom Spielen mit Kindern, von dem, was schön ist und von dem, was uns anspricht, ist ganz in Linie mit dem Trieb.
Er will sich in alles, was geht, hineinergießen, weil er die Reproduktion will und es sind wir selber, die darauf verzichten, ihm beim Handeln nachzukommen, und auf die Weise ist er nicht schädlich.
An sich ist er lebensbejahend und gibt Kraft und Inspiration, so lass dich daher durch ihn bereichern, aber lade ihn nicht mit sexuellen Handlungen auf, sondern lass ihn nach einer Weile von selber vorübergehen, und dann ist dein Fokus auf anderes im Leben.
Dies ist nichts, wovor du Angst haben brauchst.
