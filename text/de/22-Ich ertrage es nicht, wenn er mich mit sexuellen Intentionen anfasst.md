## Ich ertrage es nicht, wenn er mich mit sexuellen Intentionen anfasst

<div class="handwritten">
Jetzt hätte ich gerne, dass meine Freundin ihre Frage stellen darf.
</div>

Das geht in Ordnung, bitteschön.

<div class="handwritten">
Hallo Iris, ich finde, dass ich dir diese Fragen schon gestellt habe.
Aber ich finde nicht, dass du mich verstanden hast und mir eine Antwort gegeben hast.
Irgendwo weiß ich, dass wenn du nur verstehst, was ich frage, so wirst du mir eine Antwort geben, die mir sehr helfen kann.
Aber erst, wenn du an meine wirkliche Frage herankommst.
Ich finde, dass ich überaus deutlich gewesen bin, aber du verstehst trotzdem nicht.
</div>

Ich werde mein Bestes tun, um deine Fragen zu beantworten auf meine Weise, und vielleicht findest Du selber heraus, was du wissen brauchst, um die Werkzeuge zu bekommen, die du brauchst.
Bitteschön, die Zeit gehört dir.

<div class="handwritten">
Okay.
Ich finde, dass mein Therapeut es sehr gut beschrieben hat, das letzte Mal als wir dort waren.
Er sagte, „Frauen sind empfindlich, wenn der Mann zu ihnen kommt und eine Entladung sexuellerweise haben möchte.
Und manchmal dann finden sie, dass es so anstrengend ist, dass sie abschalten.
Und sie wollen, dass der Mann zu ihnen kommen soll und einfach offen sein soll“ ...da fühlte ich mich gesehen...
Genauso ist es bei mir.
Es tut mir weh im ganzen Körper.
Es entsteht wie ein Krampf, sobald es konkret wird.
Davor ist es ganz wunderbar, aber dann werde ich steif innen, und ich schalte ab.
</div>

Das verstehe ich und es ist sicherlich gut, dass du dich darin wieder findest und dich verstanden fühlst.
Aber hilft dir das so, dass es dir sexuellerweise besser geht in deiner Beziehung mit deinem Mann? ...
Oder wie du sagst, du bist überempfindlich, weil er dich anfasst.
Dass dies einfach unangenehm ist und dass du es nicht aushältst.
Du kannst die Anziehung spüren, wenn er dich nicht anfasst, aber sobald er dich berührt, bekommst du Angst und es wird unangenehm und du gerätst in einen Krampf und kommt nicht da raus, was macht, dass alle deine Lustgefühle verschwinden, und du wirst kalt wie ein toter Fisch.

<div class="handwritten">
Nein, das ist wahr, das lindert bei mir irgendetwas, so dass ich nicht alle Schuld auf mich nehme, weil es nicht funktioniert.
Aber das hilft mir nicht mit dem Problem an sich.
Aber du hilfst mir auch nicht mit dem Problem.
Du beschreibst nur wie ich aushalten soll, du beschreibst, wie es ist, wenn man nicht diese Überempfindlichkeit hat.
Und manchmal geht es mir dabei so, dass du einfach gemein bist, wenn du sagst, dass ich einfach ertragen muss, dass ich mich so fühle wie wenn ich vergewaltigt werde... dass es so ohne Hoffnung ist...dass ich weiterhin einfach nach einer Nadel im Heu suchen brauche, dass es nichts gibt, was ich tun kann, um mein Sexleben mit meinem Mann verbessern zu können...dass ich einfach ertragen muss, wenn er mir weiterhin Angst einjagt...dass seine Hände unerträglichen Schmerz hervorrufen und dass es keine Hilfe gibt.
</div>

Oh, das ist deine Interpretation von dem, was ich gesagt habe, nicht das was ich sage.
Aber ich verstehe, dass es möglich ist, das so zu deuten, wenn du verzweifelt bist.
Verzweifel darüber, dass du keine brauchbare Antwort von mir bekommst.
Und ich werde versuchen, es so zu erklären, dass du bestenfalls es anders deuten kannst.
Ja, es ist wahr, dass du überempfindlich bist, dass du hineininterpretierst und nicht erträgst seine Fehler und Mängel, wenn er nur danach aus ist, Sex zu haben, um eine schöne Zeit zu haben.
Dass er dich anfassen will, damit du erotisch geladen wirst und du dadurch ihm entgegenkommen möchtest.
Dass er versucht, lieb und fügsam zu sein.
Und auch wenn er all das tut oder versucht, dann ist er dennoch nur in der Situation, Samenerguss bekommen zu wollen, um sich entspannen zu können und einfach zu sein.

Er weiß nicht wie das geht, erst zum Sein zu kommen, und dann das zu bekommen, was er sich wünscht.
Er weiß nicht wie das passieren soll, denn in ihm gibt es das Sein die ganze Zeit.
Und deshalb versteht er nicht, wie er zu etwas kommen soll, in dem er schon ist.
Du bist nicht in dem Sein ohne Vorbehalt, du ruhst nicht in ihm, sondern gehst in das Sekundäre, Elementare und Wählbare.
Du gerätst in Bewertungen, bezogen auf sein Wünschen.
Dieser Wunsch ist wahr und an ihm ist nichts Falsches.
Wenn du dahinter gehen könntest und verstehen könntest, dass es Liebe ist, aber stattdessen fühlst du dich vergewaltigt.
Das Primäre ist da, wenn nichts anderes da ist.
Und es ist ein Sein, in dem er ruht, genauso natürlich wie wenn er atmet.
Und da...wird ihm das unbegreiflich.
Er kann nicht etwas schaffen, was schon da ist.
Dann wird es zur Anpassung, die bewirkt, dass er verliert, sich selber zu sein.
Und dann hat niemand von euch beiden etwas davon, was du dir wünschst.
Weder du noch er.

Das ist ein üblicher Grund, warum Männer impotent werden.
In deiner Welt wird es wie: „Wenn er dies nur entfernen könnte, dann wäre es perfekt“, aber das ist nur ein Traum.
Es ist in etwa: „Wenn er nur verstünde, dann wäre das Problem gelöst“...so ist es nicht, weil du möchtest, dass er anders sein soll als er ist und du glaubst irgendwie, dass es im Bewusstsein ist, wo er dies verändern kann und das stimmt so nicht.
Es ist auf einer viel tieferen Ebene, wo er das verändern kann.
Und wäre er imstande sich anzupassen um deinetwillen, dann würdest du einen teuren Preis zahlen und ich weiß, dass du dies nicht möchtest.
Nicht wegen des Preises, sondern weil du trotzdem möchtest, dass er so sein soll wie er ist, wenn er sich selber ist.
Und weil du nicht möchtest, dass die Beziehung mit Teilen aufhört.

Kannst du verstehen, dass du Angst hast vor den Gefühlen, die in dir geweckt werden durch seine Hände, dass das ungefährlich ist und dass du hinter die Angst zu schauen brauchst, was es wirklich ist, was dir Angst macht.
Du hast in dir selber entdeckt, wenn dir durch Zuhören geholfen worden ist, dass es daran liegt, dass er bei irgendeiner Form von Ergebnis ist, dass er zum Erguss, zur Entladung kommen möchte, dass er dorthin will, indem er Samenerguss bekommt dadurch, dass er mit dir schläft, in Liebe.

Aber was ist da hinter deiner Angst?
Wie ist sie am Anfang entstanden?
Hast du etwas Übergriffsmäßiges erlebt, was du als Vergewaltigung interpretiert hast?
Oder kann das so sein, dass du irgendwann in der Pubertät als die Jungs mit dir knutschen wollten, einen Schock bekommen hast?
Oder ist es irgendeine Art von Schwierigkeit der Kommunikation in dir?
Das heißt nicht, dass es dein Fehler ist, dass es an dir liegt.
Auch nicht, dass etwas bei dir nicht stimmt.
Es ist nur so, dass wir sehr verschieden sind und die Empfindlichkeit ist auch verschieden.
Und wenn wir vergleichen ist es so leicht, einen Sündenbock bei sich oder bei dem Partner zu suchen, anstatt zu verstehen, dass es die Unvollständigkeit ist, die sich mit den Idealen verheddert.

Es ist wahr, dass du die Probleme bekommst, die du bekommst.
Aber du bist ihm kein Objekt.
Du bist die Person, mit der er es liebt Zeit zu verbringen, mit der er leben will, er hat dich als seinen Partner gewählt und will dich.
Er meint, dass dies auch den Trieb umfaßt.
Und für ihn ist es so einfach, dass man erregt wird, man schläft miteinander, und dann kommt man vom Sein in einen entspannten Zustand.
Wenn du hörst, dass er keine Bewertungen in Bezug darauf hat, ist ihm nur unreflektiert, wie das Atmen oder wie der Herzschlag, ist dies etwas, was du zu dir nehmen kannst, damit du vor seinem Trieb nicht Angst bekommst?
Ich meine nicht, dass Du Dich anpassen sollst, nur schauen, was daran so gefährlich ist, dass er unreflektiert in seinem Trieb, in seiner Sexualität ist, warum es dir Angst macht.

<div class="handwritten">
Und immer noch verstehst du mich nicht, du verstehst nicht, dass es unerträglich ist, und dass ich mir so viel Gewalt angetan habe, so viele Male schon.
Ich kann es einfach nicht mehr.
Was mache ich bloß damit, dass ich so überempfindlich bin, so überempfindlich wie es anderen Menschen geht.
Du sagst darin liegt ein Talent, aber ich erlebe das nur als Belastung.
Und wenn ich versuche, es ihm zu erklären, meinem Mann, dass es mir Angst macht, wenn er zu mir kommt, und seine sexuelle Frustration loswerden will, anstatt einfach zu SEIN...mit mir gemeinsam...dann versteht er nichts.
Er findet „ja, aber das ist ja das, was ich tue“...Aber das ist es nicht...denn wenn er mich anfasst, anwesend, ohne diesem Ziel, dann entspanne ich mich.
Aber wenn er mich anfasst, weil er etwas möchte, dann bekomme ich Angst und werde verspannt...das ist scheußlich unangenehm...aber er bemerkt diesen Unterschied nicht, doch das tue ich.
</div>

Ich glaube nicht, dass es einem Menschen möglich ist, das zu verstehen, der nicht diese Überempfindlichkeit hat, wie es ist, so überempfindlich gegenüber Berührung zu sein.
Aber ich finde, dass du es auf eine ausgezeichnete Weise gelöst hast, indem du ihn berührst wie er es gerne hat, und dass er es einfach empfängt, und er versteht, dass du nicht das gleiche Angenehme erlebst wie er.
Und es tut ihm leid, deinetwegen.

Dann begreife ich ganz im Ernst, dass du es so hast, wie du es hast.
Ich meine, dass du irgendeine Form der Fibromyalgie hast.
Sie ist eine autoimmune Krankheit, bei der das sensitive Nervensystem über ein Ängstesystem in einen Krampf gerät.
Das Problem an sich kann man besänftigen, indem man den Körper geborgen macht, so dass er nicht im Krampf sein braucht.
Und da braucht man anzuschauen welche die ungefährliche Angst ist, die Auslöser für den Krampf ist.
Man kann das gut bei der Primärarbeit machen.
Aber dann ist der Weg lang und braucht Zeit, und deshalb brauchst du immer wieder den Krampf aufzulösen, bis die Überspannung nicht wiederkehrt bis auf einzelne Vorfälle.

Kannst du das nachvollziehen? Gerade dies, dass es dir keine Hilfe ist, wenn er sich anpasst und dies bei dir nicht ausgelöst, sondern dass du freistehend von ihm dich in der Kunst übst, hinter das Ängstesystem zu gehen und es dazu zu bringen, den Krampf nicht auszulösen.

<div class="handwritten">
Aber ich sage ja, dass es nicht möglich ist...weil seine Art mich zu berühren, mir Angst macht...ich spüre seine Intention UNWOHLIG auf meinem Körper...ich genieße das überhaupt nicht...das ist ganz und gar unmöglich...es ist mir nur nur nur möglich gewesen mit Männern, die nicht so sind...im Gegensatz dazu habe ich es so lange ertragen, dass Männer mich mit der unrichtigen Intention anfassen, dermaßen, dass ich total allergisch dagegen geworden bin...das ist einfach nicht mehr möglich...ich hasse auch die Massage deswegen.
Du sagst weiterhin, dass ich etwas genießen soll, was mir so extrem unangenehm ist, dass es einfach nicht geht.
Das ist mir nicht eine Hilfe.
</div>

Noch steckst du in deiner Interpretation.
Und sie hat Vorrang vor dem, was ich sage.
Manche Männer haben diese Sensitivität als ein Talent, Frauen auf eine solche Weise streicheln zu können, dass die Frauen den Fokus bei sich selber behalten, und die da drunter liegende Intention des Mannes nicht bemerken.
Das ist oft ein Spiel, das von den Männern gespielt wird, die das können.

Und leider, da ist ein kleines bisschen Anpassung darin.
Und das führt dazu, dass der Mann irgendwann, es satt hat, bei dieser Frau zu sein.
Und sie versteht nicht warum.
Er sucht und findet eine andere.

Wir ertragen nicht die Anpassung, die man Rücksicht nennt.
Anpassung ist eine Falle, wenn sie nicht gebraucht wird.
Und wenn das eigene Leben nicht bedroht ist, dann ist sie nicht am Platz.
Die Anpassung ist in unserer Zivilisation an einem Großteil der 60% der Trennungen innerhalb einer Zeit von 10 Jahren schuld.
Und diese Falle ist so gewöhnlich, dass man es als normal ansieht, diese Forderungen an seinen Partner zu stellen.
Leider, so stellt man nicht die Verbindung her, dass es der erste Schritt zur Trennung ist, diese Forderungen zu haben und dass der Partner sich an sie anpasst.
Da wird der, der etwas verlangt, zufrieden, und der andere verliert irgendwann das Interesse.

Das Einzige, was trägt, ist gerade dies zu verstehen, dass es nicht lebensbedrohlich ist für einen selbst, diese Probleme zu haben, und sie auf irgendeine andere Weise zu lösen, als dass der eigene Partner oder die eigene Partnerin sich anpassen sollen, damit diese Probleme nicht ausgelöst werden.
Aber du kommst dir durch mich angeklagt vor, dass ich auf der Seite vom anderen stehe, und meinst dazu noch, dass ich gemein bin, wenn ich solche Sachen sage...selbstverständlich darfst du darauf reagieren... und klar kannst du gerne etwas dazu meinen und kannst es gerne ausdrücken, denn ich verstehe diese Enttäuschung, dass du alles getan hast, was in deiner Macht steht um auszuhalten.
Und trotzdem funktioniert es nicht mit dem Mann, mit dem du wirklich dein ganzes Leben verbringen möchtest.
Frage mich weiter so können wir sehen, ob es möglich ist für dich, das auf eine andere Weise zu verstehen.

<div class="handwritten">
Wenn dies das Sein ist, wie beschreibst du dann das Tun?
Oder meinst du, dass ist dies, was du vorher beschrieben hast, was ich meinem Mann erklären soll?
Das hat nämlich so geklungen, dass es an mich gerichtet war.
Aber er wird ja das, was du vorher gesagt hast, trotzdem nicht verstehen.
Das ist ja das Problem.
</div>

Es war an dich gerichtet.
Und es war nicht so gedacht, dass du es ihm erklären sollst.
Natürlich kannst du mit ihm reden über was auch immer du möchtest, worüber wir geredet haben.
Aber nicht weil ich es sage, sondern weil du das zu deinem gemacht hast, weil du das in dein eigenes umgewandelt hast und es dann aus deinem Ausgangspunkt heraus sagst, aus deiner Referenz.
Nicht dich dahinter versteckst, dass ich dir etwas sage, was du möchtest, dass er verändern soll, weil das wird nicht gehen.
Zu deiner Frage zum Sein und zum Machen:
Sexualität ist ein Trieb und er tut sich selber.
Wenn du im Sein bist so treibt dieser das Machen zum sexuellen Akt hin.
Die erotischen Gefühle sind so stark, dass sie eigene Impulse haben.
Und auf diese Weise wird Sex zu einem Spielen, bei dem beide gemeinsam dabei sind.
Sobald du eine Bewertung has...dass es auf eine gewisse Weise sein muss...und wenn das nicht so ist...bekommst du Angst und einen Krampf und es wird schmerzhaft.
Auf diese Weise bist du schon dort in dem sekundären Bewerten, was den Platz des Primären...des Seins...übernommen hat.
Wenn du dann interpretierst, dass sein Wunsch nach Sex sekundär ist, dann wirst du selber zum Opfer, zu einem Objekt und fühlst dich gekränkt und vergewaltigt...aber wenn du verstehst, dass er im Sein ist und erotische Impulse bekommt, die den Beischlaf wollen, dann verstehst du vielleicht, dass er, da er dich liebt, im Sein ist und das tut, wozu ihm die Impulse kommen...ist das möglich zu verstehen?

Nicht, dass ich auf seiner Seite stehe, aber ich weiß, dass Männer oft auf eine anderen Weise funktionieren.
Sie bewerten nicht, sondern sind nur in ihrem Trieb und lässt diesen sie steuern...und auch diejenigen, die so sind wie du sie haben möchtest, sind im Grunde solche Männer.
Aber sie haben es sich anders angeeignet, um das zu bekommen, was sie wollen...oft wird das ein Erfolg und du wirst entspannt = beide zufrieden.
Dein Mann spielt dieses Spiel nicht.
Er ist einfach und direkt und natürlich in seiner Sexualität und da bekommst du Angst, obwohl es vollkommen ungefährlich ist.

<div class="handwritten">
Jetzt bist du wieder so kalt, sagst dass es mein Fehler ist und dass ich das auch ertragen sollte.
Pfui, das ist so unfair und gemein.
</div>

Ich sage nicht auf irgendeine Weise, dass er perfekt ist.
Er kann sicherlich auch seine Aufhängungen und andere Verwicklungen haben.
Und es kann sein, dass du dabei recht hast, wenn es darum geht, dass er vom Samenerguss-Bekommen eingenommen wird.
Aber dein Problem ist, dass du dabei hängen bleibst und glaubst oder möchtest, dass Männer anders funktionieren als sie es tun...und deshalb möchte ich dir einen Wink geben, dies aufzugeben.
Gebe mir keine Antwort dazu, aber spüre nach, durchdenke dieses, lass es dich begleiten und schaue, ob dein Körper sich vielleicht damit versöhnt.
Dann wirst du den Kampf und den Schmerz in diesem Fall nicht mehr haben.

<div class="handwritten">
Ja, ich weiß ja eigentlich alles, was du sagst...aber ich komme ja nie soweit, weil seine Art mich zu berühren mir Angst macht sowieso, schon am Anfang...weil das Machen fängt schon an, sobald er „geil“ ist.
Und da wird er sauer, weil ich ihm nicht entgegenkomme.
Und da willige ich ein, mich stattdessen vergewaltigen zu lassen, um seine Frustration nicht ertragen zu müssen.
Vielleicht kann ich das Anpassen sein lassen und stattdessen umdenken.
Vielleicht kann ich verstehen, dass es ungefährlich ist, und dann den Kampf sich auflösen lassen, oder ...
</div>

Ich meine, wenn du probierst und übst und nicht glaubst, was ich sage, sondern in der Wirklichkeit es praktiziert, dann bekommst du eigene Erfahrung davon, was dir hilft.
Und da bist du auf der Spur, mit der Zeit es sehr gut zu haben, auch wenn es um die Sexualität geht.
Das kann lange brauchen.
Da kann es viele schnörkelige Wege einschlagen.
Aber das führt oft zu einer tieferen Schicht als zu den Bewertungen, wie es zu sein hat.
Dass es auf tausenderlei verschiedene Weisen sein kann.
Und gut und schlecht auf genauso viele Weisen.
Die Unvollständigkeit ist am Spielen mit uns, weißt du?

<div class="handwritten">
Wie bleibe ich dabei??
Das fühlt sich wie eine Bedrohung in der Atmosphäre an, die auch von ihm ausgeht, quasi..., wenn ich nicht einwillige Sex zu haben, so geht er davon und hat Sex mit jemand anderem".
</div>

Das ist ein Hirngespinst, was du da hast.
Und du ordnest dich unter, weil du daran glaubst.
Und das ist nicht wahr.
Er kann zu jemandem anderen gehen, egal wie gut oder schlecht ihr es habt im Bett.
Das ist nicht entscheidend.
Denn wenn er fremdgehen möchte, dann geht er fremd, egal wie ihr es habt.

Denjenigen, die zu anderen hingehen, geht es oft sehr gut zu Hause im Bett mit ihrem Lebenspartner.
Und deshalb bekommen sie Lust, mehr Sex zu haben, als der Partner es haben will.
Andere geben dem die Schuld, dass es ihnen zu Hause schlecht geht, damit sie eine Rechtfertigung haben zu anderen zu gehen.
Einige haben es gerne, viele Sexpartner zu haben und andere sind mit einem zufrieden.
Wir sind nicht monogam geschaffen, und daher sind alle Varianten möglich.

Werde damit fertig.
Es gehört eher zu den Frauen als zu den Männern, einen anderen Sexpartner aufzusuchen, wenn man spürt, dass der Mann vor der Intimität flüchtet, und dass sie irgendeine Form von Beiderseitigkeit im Sex haben will.
Da gehen Frauen oft fremd.
Und das handelt davon, dass sie sich an ihren Partner anpassen.
Und der Trotz wird genauso durch die Anpassung bestimmt wie die passive Anpassung.
Es ist leicht, sich selber an der Nase herumzuführen.
Ich habe ja ein wenig von diesem Hintergrund bei der Einleitung erläutert.

<div class="handwritten">
Jetzt wirst du wieder so anklägerisch.
Ich fühle mich dann schlecht, weil ich solche Hirngespinste habe.
Und du gibst uns Frauen stattdessen die Schuld.
Das ist verdammt hart, weil du nicht verstehst, wie weh dies mir tut.
</div>

Es tut mir leid, dass das, was ich sagte, deine Schuldgefühle und dein schlechtes Gewissen ausgelöst hat.
Das ist ganz wertlos und führt nur wieder mal zu dem Opferpulli, den du oft trägst, wenn es um Sexualität geht.
Ich kann nicht deine Gefühle erschaffen.
Ich kann nur etwas sagen, was Gefühle in dir hervorbringt.
Und wenn du diese Verteidigung wegsinken lässt, so kannst du nach einer Weile bei dem, was ich sage, etwas anderes 'hören'.
Ich weiß, dass es kontroversiell ist.
Weil die ganze Zivilisation sich einig ist, dass die Frauen Opfer sind, wenn es zur Sexualität der Männer kommt.
Sogar soweit sich einig ist, dass es juristisch in Schweden nicht als Einwilligung gesehen wird, wenn die Frau nicht 'ja' gesagt hat.
Das gibt Frauen eine für sie selber belastende Macht.
Es nützt niemandem, weder Frau noch Mann, eine Vormachtstellung in irgendeiner Richtung zu schaffen.

Wir halten hier an und dann kannst du dich wieder bei mir melden, um mit mir zu „streiten“ oder weiterzubesprechen, so wie dein eigener weiterer Prozess geht.
