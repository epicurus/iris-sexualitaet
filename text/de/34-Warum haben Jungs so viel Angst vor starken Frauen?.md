## Warum haben Jungs so viel Angst vor starken Frauen?

<div class="handwritten">
Nächste Frage:
Warum haben manche Jungs vor starken Frauen so viel Angst?
Warum werden die kraftvollen Frauen so oft alte Jungfern, und finden niemanden, mit dem sie das Leben teilen?
Liegt das an ihnen oder ist etwas in der Zivilisation schief?
</div>

Um den Trieb Sexualität gibt es so viele Mythen.
Und am Anfang bauten sie auf die Angst von jemandem.
Auffassungen von Frauen wie 'das schwache Geschlecht', dass Frauen 'Zucht brauchen', dass Frauen 'ihren Platz wissen' sollen, dass Frauen, die mehr als drei Meter vom Herd sind, 'weggelaufene Frauen' sind, dass andere Männer 'Frauen, die in einer Beziehung sind, erobern wollen, weil es Status bringt' und tausend andere Vorstellungen, die aus der Bibel oder aus alten Texten von Göttersagen oder sittlichen Erzählungen, Fabeln und so weiter geholt sind.

Diese werden den meisten Kindern erzählt und Kinderfilme enthalten oft solche Stereotypen.
Und daher bekommen Kinder Bilder, wie es sein soll, und diese finden ihren Platz, wenn Kinder, die geschlechtsreif werden, sich intensiv entwickeln.

Um deine Frage zu beantworten, braucht man den Ausgangspunkt von dem Grundgedanken des Patriarchats zu verstehen.
Vor dem Partriarchat wusste man immer, wer die Mutter war, und hat sich damit zufrieden gegeben.
Vater war alle Männer, die in der Nähe waren.
Damals gab es nicht dieses Problem, was du ansprichst.
Aber als es wichtig wurde, dass Gott ein Mann ist, und dass der Mann sein Repräsentant auf Erden ist, da wurde es wichtig zu wissen, wer der Vater war.
Und das war nicht ganz leicht.
Deshalb entstand so viel Misstrauen und Angst um starke Frauen herum, die eigenmächtig sich nicht darum geschert haben, sich nur an einen Mann zu halten, sowie dass die Männer mit allen Frauen schlafen wollten, damit ihre Egogene so weite Verbreitung wie nur möglich erhielten.
Wenn du bei dem suchst, bekommst du vermutlich eine Antwort auf deine Frage.
