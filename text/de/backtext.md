BACK

Was Iris über Sexualität sagt, ist nicht spektakulär, eher unscheinbar, wie „Man kann Sexualität nicht machen, man kann nur in sie hineingehen“.
Über solche Sätze kann man hinweggehen.

Aber wenn man nicht darüber hinweggeht, dann können sie eine Kraft entfalten und wirklich etwas ändern.
Das ist mir beim Bearbeiten für die Herausgabe des Buches passiert.
Ich bin hineingegangen in das, was Iris dort sagt.
Und plötzlich konnte ich greifen, was es heißt, Sexualität nicht zu machen, sondern in sie hinein zu gehen.

Meine sexuellen Erlebnisse wurden verblüffend anders, freier, mit einem neuen Atem, leichter und einem Lachen am Ende.
Und meine Partnerin war ebenso überrascht, und sagte ganz ähnliche Dinge.
Manche Sätze von Iris sind unscheinbar ... aber sie können sich entfalten, manchmal erst nach vielen Jahren.

IRIS

Iris Johansson verbrachte ihre Kindheit auf einem Bauernhof inmitten einer Großfamilie, an die sich ein noch größeres Kollektiv mit sozialem Engagement anschloss. Besonders ihr Vater bemühte sich darum, dass sie trotz ihres autistischen Verhaltens nicht aus der Gemeinschaft ausgeschlossen wurde. Sie konnte dadurch ihren Weg finden und ein vollkommen eigenständiges Leben führen. Der Vater wusste selbst nicht, dass sie Autistin war, bemühte sich aber unermüdlich darum, dass sie in Kontakt zu anderen Menschen kam und blieb. Sie lernte mit 14 Jahren Lesen und Schreiben und arbeitete nach ihrer Schulzeit in einem Friseursalon, später als Kinderkrankenschwester und als Erzieherin in einem Jugendhaus. Sie studierte dann Religionsphilosophie, Pädagogik und Psychologie und leitete Einrichtungen für schwer erziehbare Jugendliche. Ihre Biografie „En annorlunda barndom“ ist in Schweden ein Bestseller und 2012 in deutscher Sprache erschienen (Eine andere Kindheit).
