## Ich bin meistens zu müde, um Sex zu haben

<div class="handwritten">
Nächste Frage:
Es geht mir fast immer so, dass ich zu müde bin um Sex zu haben.
Mein Mann sagt, ich sei wie ein toter Fisch, kalt und passiv und gleichgültig, lange Zeit, bevor ich mich erwärme.
Er findet, dass er alles getan hat, damit ich aufgelegt sein kann.
Er hat gekocht, er hat abgespült, er hat sich um die Kinder gekümmert und anderes.
Und dennoch sage ich, dass ich zu müde bin, um mit ihm zu schlafen.
Er versteht, dass das nicht der Grund ist.
Aber er fühlt sich so enttäuscht, dass er mich fragt:
„Möchtest du mich nicht?
Möchtest du dich stattdessen scheiden lassen, oder?
Aber ich möchte das nicht.
Ich möchte nur in einer weißen Ehe leben dürfen, aber er will das nicht, dann will er die Scheidung.
</div>

Du bedienst dich der Anpassung als Schutz, Sex haben zu müssen.
Du gehst in einen Gefühlsspiel hinein, wo du zum Opfer der Müdigkeit wirst und meinst es liegt an ihr, dass du nicht Sex haben kannst.
So ist es nicht.
Es hat irgendwie damit zu tun, dass du dir nicht selber das Vergnügen gönnst, den Genuss gönnst, die Freude gönnst, Sex zu haben.
Vielleicht erlaubst du nicht, dass der Trieb sich einschaltet, dass du erregt wirst, dass du dich nach dem anderen sehnst, das Pirren im Körper aufkommen zu lassen.
Er tut alles Mögliche, damit du dem Licht und der Freude in deinem Körper ihren Lauf lässt.
Aber es ist so wie wenn etwas dagegenarbeitet.
Gibt es so etwas?
Gib nicht mir die Antwort, aber sei zu dir selber aufrichtig wenn es um dieses geht.
Ist es so, dass du den Kick, die Süße spürst, die Macht zu haben, nein zu sagen, dadurch, dass du ein kalter Fisch bist.
Es kann daran liegen, dass du sehr viel erzogen wurdest als du klein warst, dass es keine andere Möglichkeit gab, als dich anzupassen, um ein tüchtiges Mädchen in den Augen deiner Eltern zu sein, damit sie sich als gute Eltern empfanden.
Das kommt häufig vor und führt oft zu einer Form von Frigidität.
Es kann auch etwas gewesen sein, das wie ein Schock gewirkt hat, ein Übergriff der Erwachsenensexualität bevor du dafür reif warst oder du kannst das Trauma eines Anderen übernommen haben und es ist wie deins geworden?
Suche in deiner Erinnerung und suche durch deine eigenen Sinne, dann wirst du sehen, ob du etwas findest.
