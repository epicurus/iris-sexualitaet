## Mein Mann hat nie den Impuls Sex zu haben. Ich mache immer den ersten Schritt

<div class="handwritten">
Nächste Frage:
Ich habe einen Mann, bei dem die Lust nicht häufig anspringt und er Sex haben will, und ich bin so eine, die das möchte.
Ich habe es gern, in dieser Nähe sein zu dürfen und diese Entspannung zu bekommen und diese Begegnung mit meinem Mann zu haben.
Aber immer bin es ich, die den ersten Schritt unternimmt.
Oft sagt er da: „Wollen  wir das nicht heute Abend sein lassen?“ oder „wenn es denn sein muss?... okay“.

Sein Glied wird steif und er kann es mir angenehm machen, wenn ich es möchte, aber mir fehlt seine Initiative.
Gibt es etwas, was ihn dazu bringen kann, selber gerne Sex zu haben.

</div>

Er befindet sich in der gleichen Situation, wie viele Frauen... sie haben eigentlich nichts gegen Sex, aber sie haben keinen eigenen Wunsch danach.
Sie warten auf die Initiative des anderen und geben sich damit zufrieden.
Manchmal liegt das daran, dass wir einen Instinkt haben, dass wenn sowieso keine Kinder dabei rauskommen dann kann es bis später warten.
Während für andere ist es so viel Vergnügen und so viel Genuss, das Loswerden von Ängsten und das Loswerden des Irrgefühls ist auch drin.
Das ist dann der Grund des Initiativenzuschlags.

Das Wichtige ist, dass du nicht ein Bild hast, dass es auf eine gewisse Weise sein soll, sondern dass es bei verschiedenen Menschen verschieden ist.
Und das ist natürlich, auch wenn es manchmal ungewöhnlich und es deshalb nicht normal = allgemein vorherrschend genannt wird.

Dadurch ist mein Rat, das zu genießen, wenn du es möchtest, dass du die Initiative hast, und dass dein Mann zu dir kommt und dass ihr es gut habt.
Lasse alle Konventionen fahren und lebe die Aufgabe im Verantwortlichsein in dir.
Dann wirst du zufrieden sein davor und dabei und du wirst danach zufrieden.
Geniiiieße es.
