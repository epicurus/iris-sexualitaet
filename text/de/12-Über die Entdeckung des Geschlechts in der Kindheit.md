## Über die Entdeckung des Geschlechts in der Kindheit

Okay, jetzt kommen wir dazu, wirklich über die Sex-Probleme zu reden, die die Menschen haben, und welche ihnen so viel im Zusammenleben zerstören und dieses möchte ich gerne auf meine Weise spiegeln.
Ich hoffe, du stellst all' die Fragen, auf die du eine Antwort haben möchtest, denn jetzt weißt du etwas über meine Referenz, wenn du die Antwort hörst.

<div class="handwritten">
 Ja, es berührt mich, all das, was du bis jetzt erzählt hast.
Das ist mir ein bisschen komisch, denn im Grunde weiß ich davon und dennoch weiß ich es nicht.
Es ist so schwer, wenn die ganze Umgebung von einem etwas anderes sagt und tut und obwohl man es selber besser weiß, so tut man selber auch etwas anderes.
Ich verstehe, dass ich durch diese Kultur und durch die Menschen, die um mich sind, sehr fest geprägt worden bin, aber dennoch finde ich, dass ich mehr mich selber sein sollte und sein müsste, und dieser existenziellen Angst nicht zum Opfer fallen, die so leicht durch die Sexualität eines anderen ausgelöst wird und die zu einem soo großen Problem in meinen nahen Beziehungen wird.
</div>

Okay.
Kannst du dich erinnern wie das war, als du selber klein warst.
Wann hast du angefangen, daran zu denken, dass Kinder unterschiedliche Geschlechter hatten,
dass man selber anders war als die vom anderen Geschlecht.

<div class="handwritten">
Es war als ich in der Schule anfing.
Davor im Kindergarten, war es uns allen egal, ob es Jungs oder Mädels waren, die miteinander gespielt haben.
Es gaben Jungs, die in der Puppenecke dabei waren, und es gab sehr viele Mädchen, die Fußball gespielt und die miteinander gerungen haben und die bei den Fang-, Ball- und sonstigen Gruppenspielen dabei waren.
Aber oft waren es die Jungs, die über die Regeln entschieden, und ich weiß, dass ich dies sehr gern hatte.

Als wir dann in die Schule kamen, da sollten die Mädchen eigene Zeit in der Sauna haben, wo die Mädchen nur unter sich waren, und die Jungs sollten zu anderer Zeit da sein.
Wir sollten zum Umkleiden vor dem Turnen getrennte Zimmer haben.
Und wir wurden bei verschiedenen Gelegenheiten zusammengepfercht, so dass die Mädchen unter sich und die Jungs unter sich waren.
Da weiß ich, dass ich anfing, die Jungs mit anderen Augen anzuschauen als ich es früher getan hatte... dann... ganz plötzlich, wurde im Menschsein, im Kindsein mir etwas fremd, es entstanden zwei verschiedene Gruppen, Jungs und Mädchen.
Ich glaube nicht, dass mir klar wurde, dass es mit dem Geschlecht zu tun hatte.
Aber irgendwie sollte man auf eine gewisse Weise sein, wenn nur Mädchen da waren, und anders wenn sowohl Mädchen als auch Jungs gemeinsam da waren.

</div>

Es ist das erste Zeichen der Entwicklung, dass einem bewusst wird, dass es zwei verschiedene Arten von Menschen gibt und dies hat in manchen Zusammenhängen eine Bedeutung, aber meistens hat es das nicht.
Wenn wir gemeinsam Dinge als Menschen tun, hat es keine Bedeutung, nur beim Kulturellen trennt man sie.

Bei der Kameradschaft unter Kindern wird zwischen Jungs und Mädchen kein Unterschied gemacht, sondern sie sind beim Miteinander ungezwungen.
Aber es gibt schon mädchenhafte Mädchen, die Jungsspiele nicht verstehen, und es gibt Jungs, die finden, dass Mädchenhaftigkeit lächerlich ist, und irgendwie unheimlich.
Die Frage ist, woher sie diese Vorurteile aufgenommen haben.
