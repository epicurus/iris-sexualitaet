## Eifersucht ist kein Zeichen für Liebe

Ein Gedankenfehler, der sehr häufig vorkommt, ist dass man glaubt, dass Eifersucht ein Beweis der Liebe ist.
So ist es nicht.
Es ist nur ein Ängstesystem, das Interesse und das Beisammensein mit dem anderen zu verlieren, was die Eifersucht auslöst.
Die Kunst ist also, nicht darauf anzuspielen... mit anderen zu flirten, für andere zugänglich zu sein bis der eigene Partner oder die eigene Partnerin finster im Sinn wird.
Durch die Eifersucht des anderen selber einen Kick zu bekommen und sich dadurch geliebt zu fühlen, dem fehlt der Wirklichkeitsbezug.
Es ist nur ein Gefühlsspiel.

<div class="handwritten">
Aber sind es meistens die Männer, die eifersüchtig sind?
Und wenn das so ist, warum ist das so?
</div>

Nein, es hat mit dem Geschlecht nichts zu tun.
Es hat viel mit einem gefühlsbezogenen Spiel zu tun.
Wir möchten so gerne, dass der oder die andere uns von außen bestätigt, damit wir dadurch einen Wert bekommen.
Dass der oder diejenige uns liebt, sodass wir darin Geborgenheit finden können.
Diese Geborgenheit ist nur ein Schein.
Es ist in etwa das Gleiche wie wenn Menschen sich gerade kennengelernt haben.
Dann verbringen Sie nicht nur Zeit zusammen, sondern sie schlafen miteinander.
Sie gehen ins Bett, treiben Geschlechtsverkehr, um einen Beweis zu haben, dass sie ein Pärchen sind.
Das ist auch eine trügerische Grundlage.
Das beweist nichts, außer dass sie ganz naturgemäße Geschöpfe sind, die aus ihrer
Fortpflanzungslust heraus gesteuert werden.

Das hindert niemanden daran, gleichzeitig andere Sexpartner zu haben.
Und es ruft eher die Lust auf andere / Lust bei anderen hervor anstelle davon, dass es sättigt, so dass es uninteressant wird.
Außerdem ist es so, dass wenn du mit jemandem Sex hast, so 'riecht' das und das erweckt die Lust bei Fremden, die dann den Versuch starten, das Gleiche zu bekommen, und die an der Lustkrankheit leicht finster im Sinn werden.
Und sie wollen sich bei dir rächen, weil du diese Lust hervorgebracht, und dann denjenigen abgewiesen hast.
Männer machen hiermit auf eine Weise, was manchmal zu Gewalt führen kann, während die Frauen das ganz anders machen.
Sie sind wie Huldren, die den Mann in einen Gefühlsmorast hineinlocken.

<div class="handwritten">
Meinst du das Ernst?
Aber das ist ja das Gegenteil von dem, was die meisten glauben.
</div>
