## Hierarchien des Altertums sind die Grundlage unserer Gesellschaft

<div class="handwritten">
Aber das Letzte, wovon du geredet hast, das, was mit den Religionen heutzutage zu tun hat, darüber möchte ich gerne mehr wissen.
</div>

In dem Griechenland des Altertums, das zu der Zeit die höchststehende Kultur hatte, gab es Gottheiten, die Ideale waren und denen man vertraute.
Von ihnen wurde vieles erzählt, zum Beispiel von Zeus, der der größte aller Götter war, und eine Tochter durch sein Haupt geboren hat und sie war die größte Göttin.
Sie waren über das Menschliche erhaben und das Menschengeschlecht war ihnen keine Bedrohung.
Die Menschen vertrauten darauf, dass die Götter zum Wohl der Menschheit walteten.

Weiter gab es Götter, die herunterstiegen und Frauen, die durch die Götter geschwängert wurden, und auf diese Weise wurden Halbgötter geboren.
In immaterieller Hinsicht waren sie unsterblich, aber sie hatten Menschenkörper.
So kam der Gedanke auf, dass es himmlische Wesen gab.

Diese wurden zur irdischen Herrscherklasse.
Sie standen über den gewöhnlichen Menschen und deshalb sollten gewöhnliche Menschen ihnen gehorsam sein.
Das war der Anfang der Klassengesellschaft, unter der wir immer noch leben, obwohl wir versuchen sie aufzulösen, und diese Ordnung baut darauf, dass wir nicht eine natürliche primäre Rangordnung von einer erschaffenen Klasse, einer Hierarchie unterscheiden.

Das menschliche System hatte verschiedene Stände, Herrscher, die Aristokraten waren, Krieger, die ihre Leibwache waren, Philosophen die Senatoren waren und Politiker und Bürger in dem Weltlichen, die das Äußere mit ihrer Pracht bereicherten... und bei dem war die Sexualität nicht gebucht.
Sie war kein Thema.
Sie regulierte sich gemäß den Möglichkeiten und sie war der Anlaß für eine ganze Menge Eifersuchtsdramen.
Dies war unter den Männern.
Die Frauen waren hiervon befreit und dadurch gab es eine Form der Ebenbürtigkeit, selbst wenn es die Männer waren, die über das Weltliche entschieden.

Die übrigen Menschen waren Sklaven.
Sie stammten aus anderen Teilen des Landes und der Welt und man schaute auf sie wie auf Halbmenschen, die da waren, um alle Notwendigkeitsarbeit, die Drecksarbeit, zu machen.
Und je zahlreicher sie waren, umso besser.
Am liebsten sollte ihr Kinderkriegen keine Grenzen haben, denn dadurch wurden die Privilegierten noch reicher und hatten es noch bequemer.
Für die Sklaven galten ganz andere Gesetze.
Aber einige konnten durch ihr Handwerk und durch ihre Kapazität sich ihre Freiheit erwerben, um den Niedrigststehenden, aber Priviligierten, anzugehören und hieraus wurde dann die Bürgerklasse.
