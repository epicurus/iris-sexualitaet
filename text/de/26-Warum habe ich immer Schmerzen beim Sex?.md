## Warum habe ich immer Schmerzen beim Sex?

<div class="handwritten">
Nächste Frage.
Warum habe ich immer Schmerzen, wenn ich mit meinem Mann zusammen bin.
Bei den Männern scheint es so etwas nicht zu geben.
Ich habe nie einen Mann sagen hören, dass es weh tut.
Ich kann den Akt an sich genießen, aber dann bekomme ich wie einen Krampf, und wie eine unangenehme Spannung innen.
Und manchmal macht es mir schon gleich Angst, dass sie kommen könnte.
Was soll ich tun?
</div>

Es klingt nach etwas Neurologischem, dass es etwas in deinem Körper gibt, was ausgelöst wird, und was diese Probleme schafft.
Ich denke, dass jemand der Experte für das Nervensystem ist, dir helfen kann.
So gehe in erster Linie zu einem solchen Arzt und lasse die Sache untersuchen.
Dann kannst du dich wieder bei mir melden und wir können sehen, ob es einen Konflikt oder eine Erinnerung in dir gibt, die in die Quere kommt.
Aber zuerst das Physische untersuchen lassen.

<div class="handwritten">
Neue Frage:
Wenn ich mit meinem Mann schlafe, dann tut es nur weh.
Ich tue nichts als diese Schmerzen und den Krampf in meinem ganzen unteren Leib spüren und das ist nur Leiden.
Wie kann ich da rauskommen?
</div>

Ich bin auf diese Frage bei der Fallbeschreibung vorher näher eingegangen, aber kurz gesagt brauchst du zum einen vielleicht etwas Krampflösendes und Selbstanregendes zu nehmen, sodass der Krampf weg ist, wenn du mit einem Mann zusammen bist.
Zum anderen brauchst du zu verstehen, dass Angst dahinter ist und dich vielleicht in der Kunst üben, es bei dir zu haben, dass es ungefährlich ist, dass du dich davor schützen kannst schwanger zu werden, und dass du dich gegenüber Geschlechtskrankheiten schützen kannst, indem du Kondome benutzt und dadurch ist es dir vielleicht möglich, dich zu entspannen.
Zu wissen, dass Sex ein Spielen ist, und dass es oft bald sich wohlig anfühlt.
Mach von deinen Sinnen Gebrauch und erlebe die Gefühle des anderen.
Und ergib dich in sie hinein und da mag es vielleicht sein, dass der Krampf gelindert wird.

Manchmal kann es daran liegen, dass wir, als wir Kinder waren, in einen Krampf kamen, weil wir glaubten, wir würden beim Kackern kaputtgehen oder etwas verlieren, und dass dies bei der Mutter oder beim Vater Freude hervorgerufen hat.
Und diese Freude sollte ihnen gewiss nicht zukommen.
Verbissen sauer blieben wir dabei.
Daher nehme wahr, was ist bei dir?
