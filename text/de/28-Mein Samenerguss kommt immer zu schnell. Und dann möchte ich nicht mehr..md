## Mein Samenerguss kommt immer zu schnell. Und dann möchte ich nicht mehr.

<div class="handwritten">
Nächste Frage.
Ein Mann sagt „Mein Samenerguss kommt immer zu schnell.
Er kommt fast direkt.
Und dann ist es bei mir vorbei, dann möchte ich nicht mehr.
Und das, findet meine Frau, ist sehr traurig.
Und das verstehe ich.
Was soll ich tun?“
</div>

Hat es mit deiner Geschlechtsreife angefangen oder ist es später im Leben so gekommen?
Hast du Angst, dass du nicht „fertig werden wirst“ oder hast du eine Vorstellung, dass du „so schnell wie möglich fertig werden sollst“?
Ich kann mir vorstellen, dass es daran liegt, dass du den Fokus auf den Abschluss hast.
Und dass es dies ist, welcher der Höhepunkt ist, was man gewiss erreichen soll, ansonsten ist es irgendwie schlecht.
So ist es nicht.
Der Samenerguss ist nur das Abschließen.
Und das ist nicht die Rosine im Kuchen.
Es kann eine solche große Versuchung sein, den Fokus darauf gerichtet zu haben, statt den Fokus beim Partner zu haben, und bei dem Sein, bei dem, was in den gegenseitigen Gefühlsfeldern sich abspielt.
Aber konzentriere dich darauf, so verändert sich vielleicht etwas.
Nimm dir erstmal Zeit, viel Zeit anwesend zu sein, und geniesse den Sog im Körper mit seinen erotischen Gefühlen, mit den Gedanken und den Bildern, die in dir geschaffen werden, die dich zum Schaudern bringen und du verspürst starke Anziehung und Lebenskraft.
Das ist ein wichtiger Teil der Sexualität.
Dann alles Kuscheln und Streicheln und Daherreden, Albernheit und Spielen, was dem Ganzen einen Schimmer verleiht.
Danach kommt der Sex an sich.
Das heißt, dass ihr euch ineinander verschlingt, drüber und drunter, umeinander herum, auf 1000 verschiedene Weise oder auf eine Weise, die euch passt.
Dass ihr innehaltet, jedesmal wenn du spürst: jetzt - Pause.
Verbleibe, mache die Augen zu, such dich in dich selber hinein und halte inne, bis die Erregung zum Erguss hin vorbeizieht.
Macht so weiter bis ihr euch reif fühlt, gemeinsam abzuschliessen.

Wenn der Samenerguss kommen sollte bevor ihr beide soweit seid, so macht mit dem Spielen, mit den Albernheiten weiter.
Ihr könnt liebevoll weitermachen, es kehrt in irgendeiner Hinsicht wieder, in ausreichendem Ausmaß, und streichelt einander und spielt.
Du wirst die Sexualität auf eine ganz neue Weise entdecken, wenn du den Fokus nicht bei dem Abschluss, bei dem Samenerguss hast, sondern beim Sein in der Erotik.
