## Für meine Frau zählt beim Sex nur, was sie selbst will ...

<div class="handwritten">
Nächste Frage:
Meine Frau ist so egoistisch.
Für sie zählt nur sie selber, wenn es zum Sex kommt.
Mir kommt es darauf an, wie es ihr geht, und ich möchte es ihr schön machen.
Aber ihr ist es nicht wichtig, wie es mir geht oder wie es mir recht ist.
Und daher entsteht für mich keine Beiderseitigkeit, nur Sex, und daran habe ich so wenig Interesse.
Ich möchte Beziehung, ich möchte es gemütlich haben, kuschelig, nicht dass es durch einen gewaltigen Sex einfach so abgeht.
Das ist ihr Interesse.
Dann hat sie Sex gern, sonst können wir es sein lassen, sagt sie.
Wie löse ich das?
</div>

Erstens ist es wichtig, dass du zufrieden bist damit, dass du unzufrieden bist.
Denn es ist adäquat, unzufrieden zu sein.
Sie will es vermutlich so haben, wie ihr es jetzt habt.
Und du möchtest es verändern.
Wenn du unzufrieden bist damit, dass du unzufrieden bist, da entstehen nur verschiedene Streitpunkte und Krieg.
Aber wenn du zufrieden bist damit, dass du unzufrieden bist, dann könnt ihr über das Phänomen Erotik auf eine andere Weise miteinander sprechen.

Was hinter ihrem Entferntsein von dir in intimen Situationen ist, weißt weder du noch sie.
Ihr wisst nur, dass es etwas gibt, was sie davon weglenkt, in der Offenheit zu sein.
Und du kannst sie danach fragen, wenn du zufrieden bist und keine Auszahlung bei der Antwort erwartest.
Dann, vielleicht nach langer Zeit, kann sie etwas von sich selber sagen, warum sie dir nicht entgegenkommen möchte.
Vielleicht war das so bei ihren Eltern, oder sie hat gelesen, dass es so sein soll, oder von wem hat sie ihr Bild?
