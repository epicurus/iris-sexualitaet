## Ein Verbot kann eine Sucht erzeugen ...

Ich rate, dass es sich um ein Verbot handelt, um ein Verbot, in die Beiderseitigkeit mit einem anderen Menschen hineinzugehen.
Dass es dir nicht erlaubt ist, in Intimität in dir selber und in der Beiderseitigkeit zu sein = dich in Vergnügen und in Genuss mit einem anderen Menschen sexuell zu vermischen.
Und dem entkommst du durch die Selbstbefriedigung.
Das ist nichts was du wählst.
Das geschieht auf einer Autopilotebene und es ist nicht so leicht, das zu steuern.
Du brauchst dich um deinen Autopilot mit Liebe zu kümmern, weil er meint, dass er durch die Impulse, mit welchen er dich steuert, dir eine Hilfe ist.
Auch wenn es Suchtimpulse sind.
Wenn du sehen kannst, dass es die Liebe des Körpers zu dir ist, dann kann es sein, dass du licht und froh wirst, anstatt Angst zu bekommen.
Und dann kannst du einfach darin bleiben, still stehen, nichts tun, bis es dieses Mal vorübergeht.
Und noch einmal, und noch einmal, bis dieses sich von alleine in der Weise löscht, dass der Autopilot nicht mehr anspringt.

Thema könnte auch sein, Schuldgefühle wegen seines Triebs zu haben, dass es einem Übergriff auf den Partner gleichgesetzt wird, in der Weise, dass Männer ganz einfach keine Anzeichen haben dürfen, dass sie einen Trieb haben.
Frauen machen von ihrem Trieb oft gebrauch, indem sie bezaubern und verzaubern, und die Großzügigkeit des Mannes dadurch gewinnen.
Dann bekommen sie schlechte Gefühle in sich selber, wenn sie einsehen, dass sie einwilligen müssen, Sex danach zu haben.
Nicht weil der Mann sie dazu zwingt, sondern weil ihre eigenen Schuldgefühle sie zwingen.
Ich kann da völlig daneben liegen und deshalb bist es nur du selber, der herausfinden kann, in dir selber, was es ist, wovor du flüchtest.
Manchmal kann es Trotz sein, Trotz dagegen, dass du meinst, du musst dich dem anderen anpassen.
Aber das brauchst du nicht.
Weil du selber das gleiche möchtest, so kannst du in dich gehen und dir bewusst sein, dass:
oho!, yippie!, dies möchte ich.
Und dann ist es deins, und autonom dein eigenes.
Weil du es selber möchtest.
Und da gibt es nicht die Unterordnung unter dem anderen oder der anderen.
Auf diese Weise kannst du aus der Falle der Anpassung rausgehen.
Dann wird es mutual und das ist beiderseitig, ohne Bedingungen, bedingungsfrei, unbedingt und zur Freude von euch beiden.
Und es hört auf, wenn eine von euch etwas anderes machen möchte.
Und das ist ganz befriedigend.

Kann es so sein, dass es dir nicht erlaubt ist, das Geschenk entgegenzunehmen, das andere geben, indem sie dich wollen?
Dann musst du einfach entkommen und da kommt der Zwang, sich selber zu befriedigen.
Wie gesagt, ich rate nur, aber spüre nach, ob etwas in dir anklingt, sodass du ahnst, wovor du flüchtest.
Vielleicht ist es ganz anders als ich es sage.

Wenn der Sog anspringt, lass ihn dir fühlbar sein im ganzen Körper.
Schließe die Augen und genieße die Kraft des Soges.
Und bleibe in ihm bis er von selber erläscht.
Das tut er, auch wenn du dich selber nicht befriedigst.
Probiere es morgens, wenn du aufwachst, und der Sog ist da.
Bleibe liegen und lege die Hände auf das Herz, spüre die Wärme, die unter den Händen entsteht.
Sei in dem und genieße die Lebenskraft, die du im Körper erscheinen spürst.

Abends, wenn die Frau kein Interesse an Erotik gerade hat, dann tue das gleiche.
Leg die Hände auf das Herz, verlebe die Zeit damit, die erotische Kraft zu genießen.
Lass sie da sein und dich inspirieren.
Sie geht vorbei und du schläfst wohl ein.
Manchmal weckt sie sicherlich auch die Lust der Frau neben dir.
Dann wird es vielleicht anders und das ist erfreulich für euch beide.

Wenn du trotzdem spürst, dass du gezwungen bist, dich selber zu befriedigen, dann kann es sein, dass du die Hilfe eines KBT Therapeuten brauchst.
Dann kannst du Übungen bekommen, die dich ableiten, damit du mit der Zeit befreit wirst von dem Zwang an sich.
Es ist eine psychische Aufhängung, eine Sucht, genau wie alle anderen Süchte.
Und das an sich kann ein Entprogrammieren nötig haben, um so ausgelöscht zu werden.

<div class="handwritten">
Danke für die Antwort.
Ich werde es dem Menschen weitererzählen, der gefragt hat.
</div>
