## Mein Glied will nicht mehr aufrecht stehen. Was kann ich tun?

<div class="handwritten">
Nächste Frage.
Ich bin ein Mann in meinen besten Jahren, aber mein Geschlecht wird nicht steif,
mein Glied will nicht mehr aufrecht stehen.
Was kann ich da tun?
</div>

Wenn es nicht an mangelnder Lust an Sex liegt, so brauchst du zu einem physischen Arzt zu gehen und dich untersuchen zu lassen.
Es gibt eine ganze Reihe von Gründen, zum Beispiel verschiedene Stressfaktoren und Hormonausfälle und anderes.
Das ist meine erste Empfehlung an dich.
Und wenn kein physischer Grund vorliegt, dann würde ich anfangen, deine Lebensumstände anzuschauen.
Wo befindest du dich in deinem eigenen Leben?
Hast du alle deine Ziele erreicht und dir fehlt eine Richtung, sind deine Kinder ausgezogen oder du und deine Frau haben nicht mehr etwas Gemeinsames, durch das ihr die Freude teilen könnt.
Alles ist nur Routine und Rituale, nur Ausenseite und so weiter.

Erst feststellen, dass es ein Problem gibt, dann kann vielleicht ein Therapeut das passende sein, oder eine Gruppe, wo die Leute auch so ähnliche Probleme haben.
Oder am liebsten das mit deinem Partner durchnehmen und sie um Hilfe bitten, sodass ihr gemeinsam Lösungen findet und mit diesem spielen könnt.
