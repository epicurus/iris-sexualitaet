## Wie kann ich den Weg aus der Eifersucht heraus finden?

<div class="handwritten">
Was kann ich denn machen und was kann jeder, dem die Eifersucht kommt, tun?
Ich finde es so hinterhältig, denn manchmal kann sie so vollkommen jeden vernünftigen Gedanken und jede vernünftige Einsicht bei mir ausschalten.
Da werde ich einfach so wütend und will diese Person, von der ich meine, er macht sich dessen schuldig und lässt mich stehen, umbringen.
Ich sehe ein, dass es nicht an ihr liegt, aber ich werde so rasend, weil er darauf hereinfällt und von diesem Charm und dieser Schmeichelei sich bezaubern lässt.
Ich werde eigentlich wütend auf sie, weil ich denke, dass sie mir gegenüber das bewusst so macht...
Und dennoch weiß ich, dass sie das nicht tut...
Sie ist nur wie sie ist.
</div>

Das erste was du tun brauchst, ist zu verstehen, dass du Angst bekommst.
Todesangst, wie wenn es auf Leben oder Tod wäre, wenn dein Freund dich verlassen würde.
Oft haben wir diesen Schrecken dadurch bekommen, dass wir durch unsere Mutter oder unseren Vater bedroht worden sind... „dann kommst du nicht mit“, als wir klein waren, oft vor dem Alter von 10 Jahren.
Dann ertragen wir nicht gefühlsmäßige Bedrohungen, verlassen zu werden.

Außerdem meinen wir, dass der andere uns umbringen wird, wenn wir einen Fehler machen und wir einen Elternteil nicht besänftigen können, wenn dieser aggressiv wird.
Es ist nicht möglich für ein Kind zu verstehen, dass der Elternteil dann Angst hat, sondern wir meinen, dass er auf uns böse ist.
Auch wenn Eltern unter sich Spiele treiben, bekommen wir Angst, dass sie uns verlassen werden, und da versuchen wir, recht zu sein, lieb zu sein und uns auszulöschen, um zu genügen.
Das bleibt dann hängen.
Und das findet oft eine Form in einer Paarbeziehung.

Die Spuren, die dies zurücklässt, sind so tief, dass wenn wir in einer Beziehung mit einem Partner sind, vor dem wir offen und verletzlich sind, und der gerade das Licht in unserem Leben ist, dem wir vertrauen, dann... befällt uns die gleiche Finsternis und wir reagieren genauso primitiv wie damals als wir Kinder waren.
Wir versuchen auf eine Unmenge von Weisen dem zu entkommen, Wege, die wir uns als Kinder angeeignet haben, um uns zu entziehen, zum Beispiel, indem wir nicht mehr fühlen.
Es ist nichts, was wir uns aussuchen, aber oft meinen wir, dass wir diese Wege einschlagen.
Die Art wie wir wählen, kommt oft von dem Autopiloten her, nicht vom Bewusstsein und dem Intellekt.

Wenn du einmal verstehst, dass du Todesangst hast, kannst du denken:
Wie kann ich mich jetzt um mich selber sorgen, da ich so viel Todesangst habe, diesen Menschen zu verlieren?
Wenn du einfach diesen Gedanken denken kannst, kannst du innen dich anders verhalten, und dir gestehen wie es in Wirklichkeit ist... und gerne auch deinem Partner... „ich fürchte um mein Leben, wenn du so bezaubert wirst von ihr.
Ich glaube dann, dass ich dich an diesen Menschen verlieren werde, und da ist so viel Scham dabei, dass ich so egoistisch denke, und es fällt mir so schwer, dazu zu stehen, dass ich auf diese Weise reagiere.
Ich möchte nicht, dass du dich nach mir richten sollst und nicht, dass du es anders machst, möchte nur, dass du weißt, dass mir die Eifersucht kommt, und dass ich mich da sehr kindisch benehme“.

Dann kann es sein, dass du jemanden brauchst, bei dem du entladen kannst, jemanden, der zuhören kann, bei dem du deine Gefühle wiederkäuen darfst, bist du etwas findest, was entschärft und du aufwachsen und heranreifen kannst, so dass du nicht wieder und wieder in diese Falle hineingehst.
Es klappt meistens nicht gleich, aber wenn du dabei bleibst und dich selber mit deiner Eifersucht konfrontierst, ist es oft so, dass sie schneller vorbeigeht und irgendwann dich nicht mehr besonders hart trifft.
