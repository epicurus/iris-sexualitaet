## Wer ist verantwortlich für Sexprobleme?

<div class="handwritten">
Nächste Frage:
Wessen Verantwortung ist euer Sexproblem?
</div>

Verantwortung ist kein Produkt, das man auf sich nehmen kann oder wo man dem anderen Menschen die Schuld geben kann, dass er nicht Verantwortung auf sich nimmt.
Verantwortung ist ein Zustand innerhalb uns selber.
Der Zustand wächst und vertieft sich anhand unserer Erfahrung und unserer Kenntnisse und daher haben beide die Möglichkeit, frei verantwortlich zu sein, das zu tun, was sie tun können.
Dann kommt ein Gefühl der Freiheit auf, und der Körper wird licht und zufrieden.
Danach streben wir das ganze Leben lang.
Es ist der gleiche Zustand, von dem wir eine Erinnerung haben aus der Zeit in der Gebärmutter, und da vor allem aus den ersten 14 Tagen, wo wir am Träger nicht angebunden sind, sondern frei im Eileiter schweben.
Wenn es zu dem Trieb Sexualität kommt, da haben wir selber zu regulieren, so dass es unserem Partner nicht Angst macht, ... wenn es uns möglich ist.
Gerade dies, Angst zu bekommen vor einem Menschen in einer sexuellen Situation, macht, dass die Lust weggeht und ist oft der erste Schritt davon, dass die Beziehung ein Ende nimmt.
Und daher: Lebe verantwortlich, indem du selber zufrieden bist und sei liebevoll und sanft fürsorglich deinem Partner, denn wie du weißt, gibt es bei dem Liebesgeschehen eine grosse Offenheit.
Dann wird das gemeinsame Leben am besten.
