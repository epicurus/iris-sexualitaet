## Ich täusche den Orgasmus vor, und er merkt es nicht

<div class="handwritten">
Nächste Frage:
Ich mag Sex nicht.
Mir fällt es leicht, erregt zu werden, aber dann wenn wir miteinander schlafen, dann möchte ich nur, dass es ein Ende hat.
Und da tue ich so, wie wenn ich einen Orgasmus habe, weil dann weiß ich, dass er selber bald fertig sein wird und Samenerguss bekommt, und dann kann ich mich umdrehen und einschlafen.
Manchmal finde ich es auf diese Weise so sinnlos, aber ich wage es nicht, etwas darüber zu sagen, denn ich mag ihn, und wir haben es sehr nett miteinander, wir fühlen uns wohl, es ist gemütlich, Dinge gemeinsam zu unternehmen.
Ich bekomme Angst, dass wenn ich nicht Sex mit ihm haben möchte, dann wird er mich verlassen, denn ich weiß, dass Sex haben für ihn wichtig ist.

Es wundert mich auch, dass er das nicht merkt, dass es nur vorgetäuscht ist.
Er meint wirklich, dass ich einen Orgasmus habe.
Das verachte ich auch, dass er mich nicht dabei entlarvt.

</div>

Es ist sehr häufig so, dass wir Frauen es so tun.
Wir spielen ein gefühlsmäßiges Spiel, was zu Folge hat, dass wir eine Bumerangwirkung bekommen.
Das, was wir da tun, macht uns enttäuscht, und da treffen uns Schuldgefühle und schlechtes Gewissen.
Um das loszuwerden bekommen wir Verachtung gegenüber dem anderen, der uns ernst nimmt und uns glaubt.
Die Verachtung gilt uns selber, aber wir kehren das zum anderen hin um, und oft... damit wir auffliegen, genau wie du sagst.
Wir möchten den Druck ablassen, aber wir verstehen das nicht.

Männer können nicht das Gleiche tun.
Ihr Glied wird ganz einfach nicht steif und deshalb geht es nicht, sie müssen dazu stehen, dass sie von der Frau nicht angezogen sind...sie können das nicht auf die Weise vortäuschen.
Stattdessen werden sie oft wütend und gewaltsam und wenn sie spüren, dass sie Macht bekommen, dadurch dass die Frau Angst bekommt, dann wird ihr Glied steif und sie kriegen ein Kick davon, und da können sie den Sex durchführen und da nennt man es Vergewaltigung.

Der Grund weshalb unser Trieb so in Gefühlsspielen umhüllt ist, ist, weil er ein Trieb ist und weil er eigentlich genauso natürlich ist wie der Herzschlag und das Atmen, aber... über die Religion ist der Trieb in Verstrickungen geraten und eine Folge von ihm ist, dass wir nach Bewertungen, die wir Menschen haben, zu viele Kinder gebären.
In der Natur gibt es sowas nicht, dass es zu viele werden.
Das heißt nur, dass die Schwachen sterben und dann gibt es einen stärkeren Menschenstamm.
Aber wir haben ein Menschenbild, alles Leben zu bewahren, und wir sind mit dem Tod auf die Weise nicht versöhnt wie die Ursprungskulturen es waren.
Deshalb kann es wichtig sein zu verstehen, dass es wir Menschen sind, die es uns selber verwickelt machen und dass dies über die Probleme der Sexualität sich am deutlichsten zeigt.

Möchtest du umdenken oder möchtest du nur eine Freundschaft mit deinem Partner haben?
Dies brauchst du zu durchdenken, um herauszufinden, wodurch es dir gut ergeht, und gemäß diesem zu leben, ohne Schuldgefühle oder schlechtes Gewissen zu haben, denn das hat keinen Wert.
Der Wert ist in der Beziehung, wie auch immer sie ist.
