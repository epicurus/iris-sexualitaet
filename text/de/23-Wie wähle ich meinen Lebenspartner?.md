## Wie wähle ich meinen Lebenspartner?

Hast du noch weitere eigene Fragen oder wollen wir an dieser Stelle stehen bleiben?

<div class="handwritten">
Doch, ich habe etliche weitere Fragen von anderen bekommen.
Aber es sind Fragen, wozu ich mir auch Überlegungen gemacht habe.
Und ich habe es vor, sie direkt zu stellen.
Und hoffe, du gibst irgendeine, verschlauernde Antwort darauf.

Zum Beispiel, wenn ich meinen Partner wähle, was spielt dabei eine Rolle?
Ich meine, dass ich beobachtet habe, dass ich als Frau oft Ähnlichkeiten habe mit der Mutter des Mannes.
Und dass er, für den ich mich entscheide, meinem Vater ähnelt...
das kann ja gut sein, aber es wird ja leicht so, dass man dann irritiert wird wegen der Person, gerade weil es einen an seinen Vater oder seine Mutter erinnert.
Ist dies wirklich ein geeigneter Grund für eine Paarbeziehung?

</div>

Es ist ja oft so, dass wenn wir einen Partner wählen gerade aus der Lust, so wird diese Lust oft hervorgerufen durch das, was bekannt und gewohnt und geborgen ist.
Und das, was uns am geborgensten ist, sind die Mitglieder der Kernfamilie.
Früher war es die Verwandtenfamilie, oder die Herde.
Deshalb sind wir oft geprägt durch die Art unserer Mutter oder unseres Vaters.
Und oft ist es dies, was die Anziehung hervorruft.

Leider ist es so, dass wir genauso durch das Negative wie vom Positiven angezogen werden.
Weil im Grunde werden wir davon angezogen, dass wir von dem beim Anderen angezogen werden, was uns vertraut vorkommt...dass der andere uns versteht, und...endlich...fühlen wir uns gesehen und verstanden.
Leider ist es so, dass wenn der andere oder die andere den gleichen Mangel hat wie ich selber, so fühlt es sich so an in der Verliebtheit, aber wenn die Verliebtheit zu Ende gegangen ist, dann wird es nur leer.
Das Risiko besteht dann, dass man enttäuscht und kritisch wird gegenüber dem, was am Anfang die Lust hervorgerufen hat.
Und dann fangen die Probleme und die Gefühlsspiele an...und sie verbinden uns genauso sehr wie die Bereicherung und das führt zu einem langwierigen Drama.

<div class="handwritten">
Aber, ihhh, wie soll man dann machen?
</div>

Ja.
Zum Beispiel man kann an all die Leute denken, die man kennt und sich überlegen, ob einer oder eine davon in Frage kommt als Vater oder als Mutter der eigenen Kinder.
Oder es gibt jemanden, mit dem man immer gerne zusammen war und der ein spannender Mensch ist.
Oder man findet jemanden, mit dem man einen Interessengebiet teilt, so dass man immer ein Thema mit jemandem teilen kann.
Vielleicht jemanden, mit dem man Segeln kann, oder jemanden, mit dem man gemeinsam den Garten bestellen kann, mit dem man tanzen kann oder so etwas.
Es gibt tausende Sachen, die das eigene Herz erwärmt.
Und der Mensch, durch den das Herz sich immer und immer wieder erwärmt, tja, vielleicht ist dieser der geeignetste nach Occam's Prinzip.

<div class="handwritten">
Was ist Occam's Prinzip?
</div>

Das Prinzip ist, dass ich aus mehreren wählbaren Alternativen eines wähle und bin der Meinung, dass es die beste Wahl ist, und dadurch sind die anderen nicht mehr wählbar.
Und ich meine, dass die Wahl, die ich getroffen habe, die unvergleichlich absolut beste ist.
Dann stehe ich dazu ohne weitere Bedingungen und greife nie das Vergleichen mit denjenigen, die ich abgewählt habe auf.
Dann habe ich die Chance in einer glücklichen, eindeutigen Beziehung, ohne gekreuzte Finger und unnötigen Zweifel zu leben und das Leben mit einem Freund zu teilen.
Freundschaft ist ja etwas, was von selber entsteht, während man Zeit miteinander verbringt.
Und dies als Grundlage einer Beziehung zu haben, ob das als Partner wird oder als eine separate Freundschaft, ist kein schlechtes Kriterium.
Die Freundschaft erträgt den anderen ganz anders als wenn die Sexualität dasjenige ist, was die Beziehung bestimmt.

<div class="handwritten">
Danke dir...
</div>
