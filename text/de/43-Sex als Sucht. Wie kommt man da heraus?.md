## Sex als Sucht. Wie kommt man da heraus?

Wenn man eine Neigung zur Sucht und einen ständigen Sog nach etwas von außen hat, da ist es einfach, zum Sex-Süchtigen zu werden.
Man gießt seinen Sog in die eigene Lust, in den eigenen einzigen Trieb, den wir als Menschen haben, in die Sexualität, und wird dann davon okkupiert, unterwegs zum Sex hin, unterwegs vom Sex weg oder auf der Transportstrecke dazwischen zu sein.
Dann ist es nur das, was gilt und was das Wichtigste ist.
Alles andere ist nur eine Pflicht zwischendrin und muss geschehen, aber ganz ohne in sich selber zu sein und ohne das Leben als den Lebenssinn zu erleben, und dass das Leben einen gegebenen Wert hat.
Wenn die Person dann eine Familie und Kinder hat, dann bleiben sie ohne Beziehung, Beiderseitigkeit und tiefen Kontakt, weil die ganze Zeit etwas dazwischen steht, und daher gibt es den Kontakt nur ab und zu für Augenblicke.
Wenn es einem selber zum Problem wird, ist das vielleicht Motiv genug zu verstehen, dass Hilfe nötig ist.
Vielleicht sucht man da Hilfe für die eigene Sucht und bekommt zum einen dann psychologische Hilfestellung, zum anderen kann man ein Teil einer Hilfegruppe anonymer Sexsüchtiger sein, wenn es das in der eigenen Umgebung gibt.
Und man braucht so dicht hinzugehen, bis man Zeiten hat, wo die Sucht nicht anspringt, und diese Zeiten länger und länger werden.
Man kann Jahre der Enthaltsamkeit brauchen und es kann sein, dass man ein nicht der Sucht folgender Süchtiger das ganze Leben bleibt oder zeitweise ein Süchtiger ist.

Dem folgt oft eine Art von Manie-Depression, die von dem einen Extrem ins andere geht.
Es ist das, was man heute bipolar nennt.
Leider bekommen viele Leute Medikamente, Psychopharmaka, und sie werden stattdessen apathisch, und das ist 'von der Asche ins Feuer'.
Das, was die Person braucht, ist, eine unterstützende Person während vieler Jahre.
Wenn man einmal die Sucht angekurbelt hat, so ist es nicht einfach, sie auszulöschen.
Es ist eher so, dass man lernt damit umzugehen.
Wenn man sexualisiertem Verhalten ausgesetzt war, den Übergriffen der metoo-Bewegung, wenn man erwachsen war, durch jemanden, der eine Machtposition innehat, und man willigt ein, vielleicht um Vorteile zu erhalten, vielleicht um Nachteile zu vermeiden, vielleicht weil diese Kultur Norm geworden ist, dann wird das Problem, dass man sich selber als schuldig erlebt.
Die Person empfindet sich als schuldig, verdreckt, exploitiert, prostituiert, und das setzt das eigene Selbstgefühl einem Übergriff aus und kränkt das eigene Vertrauen in einen selber und in die eigene Kapazität.
Das ätzt sich tief in das eigene Innere hinein.
Es sind öffentliche Geheimnisse, von denen die Umgebung oft weiß, aber alle schweigen still.
Das zehrt an der Psyche auf eine Weise, die langfristig unerträglich ist, und das braucht entsprechend kräftig entladen zu werden und seine Sprache zu bekommen, so dass man Einhalt der ganzen alteingesessenen Kultur bieten kann, die sich gerade an diesen sexualisierten Verhaltensweisen entwickelt hat.
Die Individuen brauchen Hilfe und die Gesellschaft hat es nötig, dies als ein destruktives Verhalten zu verinnerlichen.
