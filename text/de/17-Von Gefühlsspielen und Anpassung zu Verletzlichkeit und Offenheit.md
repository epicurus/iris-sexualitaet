## Von Gefühlsspielen und Anpassung zu Verletzlichkeit und Offenheit

<div class="handwritten">
Die meisten Menschen, die ich kenne, die sagen, sie machen beim Sex eher mit als sie es selber wollen, damit der Mann das Interesse an ihnen nicht verliert.
Und dann bekommen sie Widerstand, gerade weil sie sich dazu gezwungen fühlen mitzumachen, gerade weil sie an diesem Mann ein Interesse haben.
Sie denken gar nicht daran, dass dies Erwartungen beim Mann hervorruft, weil sie am Anfang eingewilligt haben... 
und sie wünschen, dass er sie respektiert, indem er nicht Sex haben will, sondern erstmal Interesse an ihnen als einmaligem Menschen hat.
Kann es so ein Fehldenken sein?
</div>

Ja, ich weiß, und deshalb ist es wichtig, das ohne Umschweif aufgedeckt zu bekommen, so dass wir nicht die Möglichkeiten kaputt machen, gemeinsame Zeit miteinander zu verleben und einander kennenzulernen, bevor der Trieb überhand nimmt.
Früher oder später tut er das zeitweise, aber er kann warten bis wir einander wirklich kennengelernt haben und einander in der Begegnung, in der Beiderseitigkeit wollen, was beide in der Richtung auf ein Wir hin verändert.

Mädchen hätten es nötig, von dem Trieb zu erfahren, dass dieser anspringt und dass die Männer deshalb es bei derjenigen versuchen, die den Trieb hervorruft, und dass dies mit der Einmaligkeit eines Menschen nichts zu tun hat.
Es hat nur damit zu tun, dass jemand die Lust zum Sex bei dem anderen erweckt.
Sie hätten es nötig zu lernen, dass sie den Partner genauso oft gewinnen wie verlieren, wenn sie Sex haben, und dass es ihnen viel besser gehen würde, wenn sie dann an der Stelle einfach zum Spielen übergehen würden, das tierische Verhalten brechen würden.

Auf Westgötisch, das ist ein Dialekt in Schweden, wo ich herkomme, gibt es einen besonderen Spruch, und jeder weiß was gemeint ist... „Halte die Fäustchen bei dir bis wir uns kennen“...
Es klingt deftiger im Dialekt, aber im Dialekt verstehen nur diejenigen das, die es eben kennen.
Gerade so einen Spruch zu haben, den man sagen kann und den alle kennen, das schafft den Raum gemeinsame Zeit miteinander verbringen zu können bis niemand sich anpasst.

<div class="handwritten">
Wie ist das denn bei den Jungs?
</div>

Sie fühlen sich oft gezwungen, es bei einem Mädchen zu versuchen, weil sie meinen, dass das Mädchen sie zugunsten eines selbstsichereren Jungen sonst verlassen wird.
Das führt bei ihnen auch zu Geringschätzung, weil das Verführen viel zu leicht ging.
Und da erleben die Jungs dieses Mädchen oftmals als nicht vertrauenswürdig.
„Jeder kann sie ja verführen, wenn es mir so leicht fiel“.
Diese Enttäuschung führt bei den Jungs in der Clique oft zum Angeben und diese Geschichten prägen die Art, wie man dann auf dieses einzelne Mädchen hinschaut.
Unter den Jungs kommt es so zu einem Wettkampfspiel, wer der größte Don Juan ist.

Die Mädchen entzücken, verzaubern und bezaubern und die Jungs verführen.
Der Unterschied liegt daran, dass die Mädchen sich hübsch machen, um andere Mädchen hinter sich zu lassen und die Jungs sind cool oder hart, um Mädchen zu bekommen.

Diese Gefühlspiele sind im Grunde dazu da, sich selber daran zu hindern, offen, verletzlich und stark, eins mit sich zu sein, sich selber zu sein, hier und jetzt und in der Begegnung.
Diese Intimität ist ungewohnt und ruft oft Schmerz und Unbehagen hervor, weil man glaubt, dass man schwach ist, wenn man offen und verletzlich ist.
Und es ist umgekehrt: Da ist man stark wie sonst nicht und verträgt am besten die Unvollständigkeiten im Leben.
