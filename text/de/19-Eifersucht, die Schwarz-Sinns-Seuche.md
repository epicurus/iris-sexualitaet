## Eifersucht und Finstersucht

Dann gibt es noch eine Finstersucht und das ist etwas ganz anderes.
Dies ist, wenn wir in eine Sackgasse innerhalb unseres Selbsts geraten, und asozial hochoperativ werden, und da kann es sein, dass wir dem anderen tatsächlich Schaden zufügen, weil der Schutz-Verteidigungs-Mechanismus beim Autopiloten anspringt.
Da sehen wir den anderen nicht als Menschen, sondern nur als Bedrohung, als einen Gegenstand an, den wir aus dem Weg räumen müssen.

Aber, um bei der normalen Eifersucht weiterzumachen...
Es ist leicht, den Fehler zu machen, dass der andere es mit dir so gut meint, dass er das, was bei dir die Eifersucht auslöst, sein lässt.
Da ist es auf der Oberfläche weiterhin ruhig, aber nicht in der Tiefe.
Da steigert sich das Misstrauen und dir kann das Hirngespenst kommen, dass dein Partner dich hintergeht.
Da geht die Beziehung auf jeden Fall kaputt.
Das einzige, was nachhaltig ist, ist, dass man selber dieses magische Denken brichst, und sich dem Schrecken stellt, um so die Eifersucht ausebben zu lassen.

Oft liegt eine verborgene Attraktion darin, eifersüchtig zu sein.
Da ist die Person hier und jetzt, da spürt sie in der Wut die Lebenskraft, da fühlt sie sich lebendiger als je, auch wenn es so sehr schmerzt und so furchtbar unangenehm ist.
Daher gibt es auch einen Widerstand, diesen Strick loszuwerden.
Dies ist schwer zu verstehen, denn wenn es um die Eifersucht geht, fühlen wir uns oft fremd uns selber gegenüber.
Es fühlt sich dann so an, als wären wir jemand anderes als wir selber.

<div class="handwritten">
Ist das dein Ernst... dass ich dies aus einem mir unbekannten Grund aufrechterhalte?
</div>

Leider sind wir manchmal so undienlich, auch wenn es unlogisch ist und unmöglich das bei sich selber zu begreifen.
Es ist wie wenn es eine destruktive Kraft wäre, die uns von außen vorgibt, was wir tun sollen.
Aber das ist es nicht.
Es hat mit unserem eigenen Lustzentrum zu tun, aus dem heraus die Anziehung entsteht, und das Dafür oder Dagegen demgegenüber, was sich da formt.

Manchmal ist es so, dass Eifersucht dadurch ausgelöst wird, dass es dem anderen gut geht, dass er oder sie zufrieden, froh und... in dem Moment offen und verletzlich ist.
Es wird so schmerzhaft, weil es nicht möglich war, in der eigenen Kindheit es so zu haben.
Und oft ist es so, dass das, was uns am meisten bei dem eigenen Partner gefällt, wollen wir nicht, dass er oder sie haben soll, weil andere auch diese Eigenschaft gern haben.
Das ist ein schwieriges Dilemma.

Eifersucht hat so viele Gesichter, dass ist unmöglich ist, anders als ganz individuell auf die jeweilige persönliche Frage einzugehen, und ich hoffe, dass alle mit ihren eigenen Erfahrungen und Geschichten beitragen können, um dieses Thema zu bereichern.
