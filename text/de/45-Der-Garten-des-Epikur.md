## Fragekarten

Dieses Heft ist aus Gesprächen entstanden und es können weitere Gespräche daraus folgen ... und das Heft darf dadurch anwachsen.
Tragt gerne eure weiteren Fragen auf der nächsten Seite „Offene Fragen“ ein.
Dort findet ihr einen QR code.
Fragen zum Thema Irrgefühl können in Workshops mit Iris dann weiter bearbeitet werden.

Die Karten mit Fragen auf den folgenden Seiten sind als Hilfestellung gedacht, falls einige von euch Lesern sich gerne physisch oder virtuell trefft, um über die Phänomene im Heft gemeinsam zu sprechen.
Wenn da jemandem keine eigene Frage an die anderen einfällt, kann eine Karte gezogen werden (sie können ja herausgeschnitten werden und gestapelt bereitliegen).
Es ist ja auch möglich, auf die Karten zu verzichten und zu hoffen, einem fällt keine Frage ein, und dann zu schauen, ob der Leistungsdruck spürbar wird, viellicht sogar das Irrgefühl vor der Leistung aufkommt.

\* Über Jahre hinweg hat Iris im Sommer täglich eine Fragestunde unter einem Baum gehalten.
Sie saß in der Mitte und um sie herum, Kinder, junge Erwachsene, Ältere, die gefragt haben, was sie wollten und Iris hat geantwortet.

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Was ist Sexualität für dich?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Was bedeutet dir die Sexualität?
</div>

<!-- if booktype is pdf -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- endif booktype -->

<div class="handwritten">
Was ist dir bei der Sexualität nicht erlaubt?
</div>

<!-- if booktype is pdf -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- endif booktype -->

<div class="handwritten">
Wie kennst du die Sexualität: Was ist an ihr irdisch, was an ihr göttlich?
</div>

<!-- if booktype is pdf -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - wie du vor der Sexualität Angst hast.
</div>

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - wie du erst Eigenschaften, Phänomene bei jemandem geliebt hast und wie es dann war, als dies dir lästig wurde.
</div>

<!-- if booktype is pdf -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- endif booktype -->

<div class="handwritten">
Wie ist dir zumute, wenn du daran denkst, deine Eltern würden dir den Partner aussuchen?
Er wäre dir dann gegeben und du würdest mit ihm oder ihr leben, vielleicht Kinder bekommen.
Wenn du dich in dieses Leben einfühlst:
Wie wäre dieses Leben?
Wie wäre dir dabei?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Wie spielten die kulturelle Vertrautheit und die erotische Anziehung mit herein als du und dein Partner euch fanden?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle -  wenn du magst - wie deine Sexualität durch deine kulturellen Gebote zurückgehalten wird.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - wie die Verhältnisse waren als du nebenbei einen Liebhaber, eine Liebhaberin hattest.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Ist einer von uns mit Menschen aufgewachsen, die religiös waren oder ihrerseits mit religiösen Eltern aufgewachsen sind?
Erzähle - wenn du magst - , was du als "Anleitung" in Bezug auf den Umgang mit der Sexualität mitgenommen hast.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Welche Rolle und welcher Platz hatte dein Vater in deiner Familie?
Wie wurde er erhoben, wie wurde er herabgesetzt?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Welche Rolle und welcher Platz hatte deine Mutter in deiner Familie?
Wie wurde sie erhoben, wie wurde sie herabgesetzt?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Ist es dir möglich die Hierarchie in deinem Leben wegzudenken?
Erzähle, wenn du magst, wie ein Leben ohne Hierarchie wäre.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Was bedeutet die Monogamie für dich?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Was ist für dich Treue in einer Beziehung?
Was bedeutet dir Treue?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - von einer Situation, wo du auf einen anderen Menschen in Bezug auf deinen Partner eifersüchtig warst.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - welche Bewertungen du mit der Sexualität verknüpfst.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - deinen Traum davon, wie dein Partner sein soll.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - deinen Traum davon, wie du sein sollst um den Traum deines Partners zuerfüllen.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Welche Erwartung spürst du gelegentlich von deinem Parter/ deiner Partnerin?
Beschreibe - wenn du magst - deine Reaktion und was du sonst noch spürst.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Welche sexuelle Erwartung spürst du gelegentlich von deinem Parter/ deiner Partnerin?
Beschreibe - wenn du magst - deine Reaktion und was du sonst noch spürst.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - von einer Erfahrung, wo der Trieb sich bei dir eingeschaltet hat, und es war „unpassend“ oder „unmöglich“.
</div>

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<div class="handwritten">
Suche in deiner Erinnerung.
Kannst du eine Erfahrung finden, bei der du im Beisammensein mit jemandem anwesend warst,  und es war dieses Gemeinsame, diese gemeinsame Bewegung, dieses Gegenwärtigsein im Tun, hier und jetzt?
Oder ein Hauch davon.
Teile etwas davon mit den anderen -  wenn du magst.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Welche Tabus in Bezug auf die Sexualität könnten in unserer Zeit und in unserer Kultur als „Naturgesetze“ sinnvoll sein?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Hast du mit den „dunklen“ Triebsimpulsen eine Erfahrung gemacht?
Magst du davon erzählen?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erinnere eine Situation, wo dein Trieb sich eingeschaltet hat und dir war das peinlich.
Magst du davon erzählen?
</div>

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erinnere eine Situation, wo der Trieb eines anderen sich eingeschaltet hat und du hast das bemerkt und nicht gewußt wie damit umgehen.
Magst du davon erzählen?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Als du in die Schule gekommen bist, wie war da dein Blick auf das andere Geschlecht?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Gab es in deiner Kindheit diese Unterscheidung zwischen Jungsspielen und Mädchenspielen?
Wenn ja, wie gab es sie und wie trat diese Unterscheidung in deinem Leben hervor?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erinnerst du dich an deinen ersten Kuss?
Wie war die Situation, wie fühlte das sich an, wie war der Nachklang?
</div>

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - was du erinnerst von der sexuellen Aufklärung durch deine Mutter oder durch deinen Vater.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle - wenn du magst - wie sich deine Sicht auf deine Eltern durch die Geschlechtsreife geändert hat.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle  - wenn du magst - von einer Gelegenheit, wo du vom gleichen Geschlecht angezogen wurdest.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Hast du Bekanntschaft mit Eifersucht bei dir selber gemacht?
Erzähle – wenn du magst – von einer Gelegenheit.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Wenn es dir schwer fiele, den Leuten deiner Gesprächgruppe zu erzählen, wie du jemanden verführt hast oder wie du dich verführen hast lassen, obwohl du eigentlich warten wolltest, was ist dann schwer damit?
Fühlst du dich nicht geborgen genug?
Brauchst du mehr Geborgenheit von außen, oder bist du nicht geborgen genug in dir?
Wie hast du vor der Intimität mit dir Angst?
Erzähle wenn du magst.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle – wenn du magst – wie du dich beim Erotischen anpasst um es deinem Partner rechtzumachen.
Wie sieht hier dein Muster aus?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Was bringt dich in die Lage lebensbejahend zu sein und wird deine Lust dadurch aufgeweckt?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Was ist bei dir ein Leisten und nicht ein Spielen, wenn du die Sexualität mit deinem Partner lebst?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Wo fühlst du dich bei der Sexualität minderwertig?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Wenn du deine Sexualität mit jemandem lebst, wovor hast du Angst?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Erzähle – wenn du magst – ob du eine unheilvolle Vorstellung vom Leben hast, eine Strafe, die immer wieder und wieder dich trifft.
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<!-- if booktype is pdf --><div class="title-page">&nbsp</div><!-- endif booktype -->
<!-- if booktype is pdf --><div style="visibility: hidden;">\pagebreak</div><!-- endif booktype -->

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Bist du gut genug?
Und wenn nicht magst du erzählen was dir fehlt?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Bist du bei deinem Partner eifersüchtig gewesen?
Magst du erzählen wie das war, wie du gefühlt hast und was du getan hast?
</div>

<!-- if booktype is pdf -->![Ausschneiden](${MEDIA_PATH}/scissors.png)<!-- endif booktype -->

<div class="handwritten">
Patriarchat und Matriarchat sind von vielen Bewertungen umgeben.
Schaue, ob es möglich ist hierüber zu reden.
Wenn ja, wie wäre für dich und die anderen in einem Matriarchat zu leben.
Wie stellt ihr euch das Leben in einem Martriarchat vor? 
Gäbe es eine Form, die weder noch oder beides ist?
Wie wäre es in ihr zu leben?
</div>

<div class="title-page">&nbsp</div>
<div style="visibility: hidden;">\pagebreak</div>
