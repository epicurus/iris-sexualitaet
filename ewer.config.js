const {
  asin,
  isbn,
  version,
  license,
  repository,
  description: title,
  authors,
} = require("./package.json");

module.exports = {
  meta: {
    version: "1.0.0",
    type: "ewer:ebook",
  },
  general: {
    bookTypesToUpload: ["rtf"],
  },
  metadata: {
    title,
    authors,
    outputProfile: "kindle",
    bookProducer: "ewer",
    comments: license,
    isbn,
    pubdate: "<DATE>",
    publisher: "Michael Schmidt",
  },
  additionalMetadata: {
    asin,
    version,
    license,
    repository,
    rawBookUrl:
      "Place a url here or let it be fill automatically with uploaded books",
    stripHeadlinesAndFormatsFrom: ["rtf", "txt"],
  },
  lookAndFeel: {
    extraCss: `${__dirname}/media/global.css`,
    embedAllFonts: true,
    lineHeight: 22,
    changeJustification: "justify",
  },
  txtInputOptions: {
    formattingType: "markdown",
  },
  mobiOutputOptions: {
    mobiFileType: "both",
  },
  epubOutputOptions: {},
  azw3OutputOptions: {
    prettyPrint: true,
  },
  pdfOutputOptions: {
    customSizeCm: "12.7x20.32", // overwrites customSize and paperSize with cm values: e.g. '12x19',
    // Amazon KDP, Standard: | 12.7x20.32 | 13.34x20.32 | 13.97x21.59 | 15.24x22.86
    // Amazon KDP, Other Standard: 12.85x19.84 | 15.6x23.39 | 16.99x24.4 | 17.78x25.4 | 18.9x24.61 | 19.05x23.5 | 20.32x25.4 | 21.59x27.94
    // Amazon KDP, Non Standard: 21x29.7 | 20.96x15.24 | 20.96x20.96 | 21.59x21.59
    // Libri BoD (www.bod.de) Sizes: 12x19 | 13.5x21.5 | 14.8x21 | 15.5x22 | 17x17 | 17x22 | 21x15 | 19x27 | 21x21 | 21x29.7
    customSize: null, // overwrites paperSize with inch values, e.g. '4.72x7.48'
    paperSize: "a5",
    pdfPageNumbers: true,
    prettyPrint: true,
    pdfDefaultFontSize: 14.5,
    pdfAddToc: true,
    pdfHyphenate: true,
    pdfOddEvenOffset: 0,
    pdfPageMarginTop: 48.0,
    pdfPageMarginBottom: 48.0,
    pdfPageMarginLeft: 48.0,
    pdfPageMarginRight: 48.0,
  },
  txtOutputOptions: {
    forceMaxLineLength: null,
    inlineToc: false,
    keepColor: false,
    keepImageReferences: false,
    keepLinks: false,
    prettyPrint: true,
    txtOutputFormatting: "plain",
  },
  rtfOutputOptions: {
    prettyPrint: true,
  },
  tableOfContents: {
    tocTitle: "Inhalt",
    level1Toc: "//h:h2",
    // level2Toc: '//h:h2',
    // level3Toc: '//h:h5',
    useAutoToc: true,
  },
};
